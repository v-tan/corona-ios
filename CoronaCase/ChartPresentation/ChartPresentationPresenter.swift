//
//  ChartPresentationPresenter.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class ChartPresentationPresenter: ViperBasePresenter, ChartPresentationPresenterProtocol {}
