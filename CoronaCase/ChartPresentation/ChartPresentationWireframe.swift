//
//  ChartPresentationWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class ChartPresentationWireframe: ViperBaseWireframe, ChartPresentationWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: ChartPresentationViewController.self,
      presenter: ChartPresentationPresenter.self,
      interactor: nil,
      apiManager: nil, localManager: nil)
    return model
  }
}
