//
//  ChartPresentationProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol  ChartPresentationViewProtocol: ViperViewProtocol {}

extension ChartPresentationViewProtocol {
  var presenter: ChartPresentationPresenterProtocol? {
    return _presenter as? ChartPresentationPresenterProtocol
  }
}

protocol ChartPresentationPresenterProtocol: ViperBasePresenterProtocol {}

extension ChartPresentationPresenterProtocol {
  var view: ChartPresentationViewProtocol? {
    return _view as? ChartPresentationViewProtocol
  }
  
  var interactor: ChartPresentationInteractorInputProtocol? {
    _interactor as? ChartPresentationInteractorInputProtocol
  }
  
  var wireframe: ChartPresentationWireframeProtocol? {
    return _wireframe as? ChartPresentationWireframeProtocol
  }
}

protocol ChartPresentationInteractorInputProtocol: ViperBaseInteractorInputProtocol {}

extension ChartPresentationInteractorInputProtocol {
  var output: ChartPresentationInteractorOutputProtocol? {
    return _output as? ChartPresentationInteractorOutputProtocol
  }
}

protocol ChartPresentationInteractorOutputProtocol: ViperBaseInteractorOutputProtocol {}

protocol ChartPresentationWireframeProtocol: ViperBaseWireframeProtocol {}

