//
//  ChartPresentationViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit
import Charts

class ChartPresentationViewController: BaseViewController, ChartPresentationViewProtocol, ChartViewDelegate {
  
  var lineChart =  LineChartView()
  var statesModel = [StateModel]()
  var states: [String] = []
  var cases: [Int] = []
  var deaths: [Int] = []
  var recovered: [Int] = []
  let data = LineChartData()
  var lineChartEntry2 = [ChartDataEntry]()
  var lineChartEntry3 = [ChartDataEntry]()
  var lineChartEntry1 = [ChartDataEntry]()
  var newArray : [IChartDataSet] = []
  
  @IBOutlet var countryNameLabel: UILabel!
  @IBOutlet var casesLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    lineChart.delegate = self
    lineChart.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width)
    lineChart.center = view.center
    view.addSubview(lineChart)
    
    for number in 0..<(statesModel.count-1) {
      states.append(statesModel[number].state ?? "")
      cases.append(statesModel[number].noOfCases ?? Int(0.0))
      deaths.append(statesModel[number].deaths ?? Int(0.0))
      recovered.append(statesModel[number].cured ?? Int(0.0))
    }
    
    for i in 0..<cases.count {
      lineChartEntry1.append(ChartDataEntry(x:Double(i), y:  Double(deaths[i])))
    }
    
    let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Death Dataset")
    data.addDataSet(line1)
    
    line1.colors = [UIColor.black]
    line1.drawValuesEnabled = false
    line1.drawCirclesEnabled = false
    
    if (cases.count > 0) {
      
      for i in 0..<cases.count {
        lineChartEntry2.append(ChartDataEntry(x:Double(i), y:  Double(recovered[i])))
      }
      
      let line2 = LineChartDataSet(entries: lineChartEntry2, label: "Recovered Dataset")
      data.addDataSet(line2)
      
      line2.colors = [UIColor.green]
      line2.drawValuesEnabled = false
      line2.drawCirclesEnabled = false
    }
    
    if (cases.count > 0) {
      
      for i in 0..<cases.count {
        lineChartEntry3.append(ChartDataEntry(x:Double(i), y:  Double(cases[i])))
      }
      
      let line3 = LineChartDataSet(entries: lineChartEntry3, label: "Cases Dataset")
      data.addDataSet(line3)
      
      line3.colors = [UIColor.red]
      line3.drawValuesEnabled = false
      line3.drawCirclesEnabled = false
    }
    self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: states)
    self.lineChart.data = data
    
    data.setDrawValues(true)
    lineChart.xAxis.granularity = 1
    lineChart.animate(xAxisDuration: 2.5)
    lineChart.xAxis.labelPosition = .bottom
    lineChart.xAxis.drawGridLinesEnabled = false
    lineChart.chartDescription?.enabled = false
    lineChart.legend.enabled = true
    lineChart.rightAxis.enabled = false
    lineChart.leftAxis.drawGridLinesEnabled = false
    lineChart.leftAxis.drawLabelsEnabled = true
  }
  
  @IBAction func deathButtonClicked(_ sender: UIButton) {
    if sender.isSelected {
      sender.isSelected = false
      
      newArray = []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        
        if "Death Dataset" == (newArray[index].label) {
          
          data.removeDataSetByIndex(index)
        }
      }
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: states)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      for i in 0..<cases.count {
        lineChartEntry1.append(ChartDataEntry(x:Double(i), y:  Double(deaths[i])))
      }
      
      let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Death Dataset")
      data.addDataSet(line1)
      
      line1.colors = [UIColor.black]
      line1.drawValuesEnabled = false
      line1.drawCirclesEnabled = false
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: states)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
    }
  }
  
  @IBAction func casesButtonClicked(_ sender: UIButton) {
    if sender.isSelected {
      
      sender.isSelected = false
      
      newArray =  []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        
        if "Cases Dataset" == (newArray[index].label) {
          data.removeDataSetByIndex(index)
        }
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: states)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      
      if (cases.count > 0) {
        
        for i in 0..<cases.count {
          lineChartEntry3.append(ChartDataEntry(x:Double(i), y:  Double(cases[i])))
        }
        
        let line3 = LineChartDataSet(entries: lineChartEntry3, label: "Cases Dataset")
        data.addDataSet(line3)
        
        line3.colors = [UIColor.red]
        line3.drawValuesEnabled = false
        line3.drawCirclesEnabled = false
        
        self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: states)
        self.lineChart.data = data
        
        data.setDrawValues(true)
        lineChart.xAxis.granularity = 1
        lineChart.animate(xAxisDuration: 2.5)
        lineChart.xAxis.labelPosition = .bottom
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.chartDescription?.enabled = false
        lineChart.legend.enabled = true
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawLabelsEnabled = true
      }
    }
  }
  
  @IBAction func cureButtonClicked(_ sender: UIButton) {
    if sender.isSelected {
      sender.isSelected = false
      
      newArray = []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
      
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        if "Recovered Dataset" == (newArray[index].label) {
          data.removeDataSetByIndex(index)
        }
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: states)
      self.lineChart.data = data
      data.setDrawValues(true)
      
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      
      if (cases.count > 0) {
        
        for i in 0..<cases.count {
          lineChartEntry2.append(ChartDataEntry(x:Double(i), y:  Double(recovered[i])))
        }
        
        let line2 = LineChartDataSet(entries: lineChartEntry2, label: "Recovered Dataset")
        data.addDataSet(line2)
        
        line2.colors = [UIColor.green]
        line2.drawValuesEnabled = false
        line2.drawCirclesEnabled = false
        self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: states)
        self.lineChart.data = data
        data.setDrawValues(true)
        lineChart.xAxis.granularity = 1
        lineChart.animate(xAxisDuration: 2.5)
        lineChart.xAxis.labelPosition = .bottom
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.chartDescription?.enabled = false
        lineChart.legend.enabled = true
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawLabelsEnabled = true
      }
    }
  }
}
