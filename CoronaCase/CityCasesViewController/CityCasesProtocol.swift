//
//  CityCasesProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 24/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol  CityCasesViewProtocol: ViperViewProtocol {}

extension CityCasesViewProtocol {
  var presenter: CityCasesPresenterProtocol? {
    return _presenter as? CityCasesPresenterProtocol
  }
}

protocol CityCasesPresenterProtocol: ViperBasePresenterProtocol {}

extension CityCasesPresenterProtocol {
  var view: CityCasesViewProtocol? {
    return _view as? CityCasesViewProtocol
  }
  
  var interactor: CityCasesInteractorInputProtocol? {
    _interactor as? CityCasesInteractorInputProtocol
  }
  
  var wireframe: CityCasesWireframeProtocol? {
    return _wireframe as? CityCasesWireframeProtocol
  }
}

protocol CityCasesInteractorInputProtocol: ViperBaseInteractorInputProtocol {}

extension CityCasesInteractorInputProtocol {
  var output: CityCasesInteractorOutputProtocol? {
    return _output as? CityCasesInteractorOutputProtocol
  }
}

protocol CityCasesInteractorOutputProtocol: ViperBaseInteractorOutputProtocol {}

protocol CityCasesWireframeProtocol: ViperBaseWireframeProtocol {}



