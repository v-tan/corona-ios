//
//  CityCasesViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 24/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit

class CityCasesViewController: BaseViewController, CityCasesViewProtocol {
  
  var cityName: String?
  var stateName: String?
  var active: Int?
  var confirmed: Int?
  var deceased: Int?
  var recovered: Int?
  
  @IBOutlet var confirmedLabel: UILabel!
  @IBOutlet var deathsLabel: UILabel!
  @IBOutlet var recoveredLabel: UILabel!
  @IBOutlet var cityNameLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    cityNameLabel.text = cityName
    guard let url = URL(string: Constants.districtApiUrl ) else { return }
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      
      do {
        if error == nil {
          let district = try JSONDecoder().decode(StateDataModel.self, from: data!)
          
          switch self.stateName {
          case "West Bengal" :
            
            self.confirmed = district.westBengal.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.westBengal.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.westBengal.districtData[self.cityName ?? ""]?.recovered
            
          case "Uttarakhand" :
            self.confirmed = district.uttarakhand.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.uttarakhand.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.uttarakhand.districtData[self.cityName ?? ""]?.recovered
            
          case "Uttar Pradesh" :
            self.confirmed = district.uttarPradesh.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.uttarPradesh.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.uttarPradesh.districtData[self.cityName ?? ""]?.recovered
            
          case "Telangana" :
            self.confirmed = district.telangana.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.telangana.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.telangana.districtData[self.cityName ?? ""]?.recovered
            
          case "Tamil Nadu" :
            self.confirmed = district.tamilNadu.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.tamilNadu.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.tamilNadu.districtData[self.cityName ?? ""]?.recovered
            
          case "Rajasthan" :
            self.confirmed = district.rajasthan.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.rajasthan.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.rajasthan.districtData[self.cityName ?? ""]?.recovered
            
          case "Punjab" :
            self.confirmed = district.punjab.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.punjab.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.punjab.districtData[self.cityName ?? ""]?.recovered
            
          case "Odisha" :
            self.confirmed = district.odisha.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.odisha.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.odisha.districtData[self.cityName ?? ""]?.recovered
            
          case "Nagaland" :
            self.confirmed = district.nagaland.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.nagaland.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.nagaland.districtData[self.cityName ?? ""]?.recovered
            
          case "Mizoram" :
            self.confirmed = district.mizoram.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.mizoram.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.mizoram.districtData[self.cityName ?? ""]?.recovered
            
          case "Meghalaya" :
            self.confirmed = district.meghalaya.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.meghalaya.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.meghalaya.districtData[self.cityName ?? ""]?.recovered
            
          case "Manipur" :
            self.confirmed = district.manipur.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.manipur.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.manipur.districtData[self.cityName ?? ""]?.recovered
            
          case "Maharashtra" :
            self.confirmed = district.maharashtra.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.maharashtra.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.maharashtra.districtData[self.cityName ?? ""]?.recovered
            
          case "Madhya Pradesh" :
            self.confirmed = district.madhyaPradesh.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.madhyaPradesh.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.madhyaPradesh.districtData[self.cityName ?? ""]?.recovered
            
          case "Kerala":
            self.confirmed = district.kerala.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.kerala.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.kerala.districtData[self.cityName ?? ""]?.recovered
            
          case "Karnataka":
            self.confirmed = district.karnataka.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.karnataka.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.karnataka.districtData[self.cityName ?? ""]?.recovered
            
          case "Jharkhand":
            self.confirmed = district.jharkhand.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.jharkhand.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.jharkhand.districtData[self.cityName ?? ""]?.recovered
            
          case "Himachal Pradesh":
            self.confirmed = district.himachalPradesh.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.himachalPradesh.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.himachalPradesh.districtData[self.cityName ?? ""]?.recovered
            
          case "Haryana":
            self.confirmed = district.haryana.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.haryana.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.haryana.districtData[self.cityName ?? ""]?.recovered
            
          case "Gujarat":
            self.confirmed = district.gujarat.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.gujarat.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.gujarat.districtData[self.cityName ?? ""]?.recovered
            
          case "Delhi":
            self.confirmed = district.delhi.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.delhi.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.delhi.districtData[self.cityName ?? ""]?.recovered
            
          case "Chhattisgarh":
            self.confirmed = district.chhattisgarh.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.chhattisgarh.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.chhattisgarh.districtData[self.cityName ?? ""]?.recovered
            
          case "Bihar":
            self.confirmed = district.bihar.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.bihar.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.bihar.districtData[self.cityName ?? ""]?.recovered
            
          case "Assam":
            self.confirmed = district.assam.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.assam.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.assam.districtData[self.cityName ?? ""]?.recovered
            
          case "Arunachal Pradesh":
            self.confirmed = district.arunachalPradesh.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.arunachalPradesh.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.arunachalPradesh.districtData[self.cityName ?? ""]?.recovered
            
          case "Andhra Pradesh":
            self.confirmed = district.andhraPradesh.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.andhraPradesh.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.andhraPradesh.districtData[self.cityName ?? ""]?.recovered
            
          case "Jammu and Kashmir":
            self.confirmed = district.andhraPradesh.districtData[self.cityName ?? ""]?.confirmed
            self.deceased = district.andhraPradesh.districtData[self.cityName ?? ""]?.deceased
            self.recovered = district.andhraPradesh.districtData[self.cityName ?? ""]?.recovered
            
          default: print("no states")
          }
          
          DispatchQueue.main.async {
            if let confirmedCases = self.confirmed , let deceasedCases = self.deceased, let recoveredCases = self.recovered {
              self.confirmedLabel.text = String(confirmedCases)
              self.recoveredLabel.text = String(recoveredCases)
              self.deathsLabel.text = String(deceasedCases)
              
            } else{}
          }
        }
      } catch {
        print("Error Found")
      }
    }.resume()
  }
}
