//
//  CityCasesWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 24/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class CityCasesWireframe: ViperBaseWireframe, CityCasesWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: CityCasesViewController.self,
      presenter: CityCasesPresenter.self,
      interactor: nil,
      apiManager: nil, localManager: nil)
    return model
  }
}
