//
//  SignUpAuthCheck.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol SignUpModuleAuthManagerInputProtocol: ApiDataManagerInputProtocol {}

protocol SignUpModuleAuthManagerOutputProtocol: ApiDataManagerOutputProtocol {}

class SignUpAuthManager: ApiDataManager,
SignUpModuleAuthManagerInputProtocol {}

protocol SignUpAuthManagerInputProtocol: SignUpModuleAuthManagerInputProtocol{
  
  func signUp(email: String, password: String)
  func googlesignedIn(idToken: String, accessToken: String)
}

protocol SignUpAuthManagerOutputProtocol: SignUpModuleAuthManagerOutputProtocol {
  
  func success(response: String)
  func failure(response: String)
  func googleSignInSuccess(uid: String)
}

extension SignUpAuthManager: SignUpAuthManagerInputProtocol {
  
  func googlesignedIn(idToken: String, accessToken: String) {
    let credentials = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
    
    Auth.auth().signIn(with: credentials) { (user, error) in
      if let error = error {
        print("Failed to create Firebase user with Google Account", error)
      }
      
      guard let uid = user?.user.uid else { return }
      print("Successfully Logged into Firebase",uid)
      
      self.output?.googleSignInSuccess(uid: uid)
    }
  }
  
  var output: SignUpAuthManagerOutputProtocol? {
    return _output as? SignUpAuthManagerOutputProtocol
  }
  
  func signUp(email: String, password: String) {
    
    Auth.auth().createUser(withEmail: email , password: password ) { (result, error) in
      
      if error != nil {
        
        self.output?.failure(response: error?.localizedDescription ?? "")
      } else {
        self.output?.success(response: result?.user.uid ?? "")
      }
    }
  }
}
