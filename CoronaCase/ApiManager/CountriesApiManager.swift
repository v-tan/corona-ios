//
//  CountriesApiManager.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation
import Alamofire

protocol CountriesModuleApiManagerInputProtocol: ApiDataManagerInputProtocol {}

protocol CountriesModuleApiManagerOutputProtocol: ApiDataManagerOutputProtocol {}

class CountriesApiManager: ApiDataManager,
CountriesModuleApiManagerInputProtocol {}

protocol CountriesApiManagerInputProtocol: CountriesModuleApiManagerInputProtocol {
  
  func countryList()
}

protocol CountriesApiManagerOutputProtocol: CountriesModuleApiManagerOutputProtocol {
  func success(response: [CountryModel])
}

extension CountriesApiManager: CountriesApiManagerInputProtocol {
  
  var typeOutput: CountriesApiManagerOutputProtocol? {
    return _output as? CountriesApiManagerOutputProtocol
  }
  
  func countryList() {
    
    let request =  AF.request(Constants.countryUrl)
    request.responseDecodable(of: [CountryModel].self) { (response) in
      
      if let value = response.value {
        self.typeOutput?.success(response: value)
        
      } else {
        print("Api Fails")
      }
    }
  }
}
