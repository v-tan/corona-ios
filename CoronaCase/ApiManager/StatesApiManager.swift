//
//  StatesApiManager.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation
import Alamofire

protocol StatesModuleApiManagerInputProtocol: ApiDataManagerInputProtocol {}

protocol StatesModuleApiManagerOutputProtocol: ApiDataManagerOutputProtocol {}

class StatesApiManager: ApiDataManager,
StatesModuleApiManagerInputProtocol {}

protocol StatesApiManagerInputProtocol: StatesModuleApiManagerInputProtocol{
  
  func stateList()
}

protocol StatesApiManagerOutputProtocol: StatesModuleApiManagerOutputProtocol {
  
  func success(response: [StateModel])
}

extension StatesApiManager: StatesApiManagerInputProtocol {
  
  var typeOutput: StatesApiManagerOutputProtocol?{
    return _output as? StatesApiManagerOutputProtocol
  }
  
  func stateList() {
    let request =  AF.request(Constants.indianStateUrl)
    request.responseDecodable(of: [StateModel].self) { (response) in
      
      if let value = response.value {
        self.typeOutput?.success(response: value)
        
      } else {
        print("Api Fails")
      }
    }
  }
}
