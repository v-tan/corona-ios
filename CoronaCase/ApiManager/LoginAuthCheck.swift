//
//  LoginAuthCheck.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol LoginModuleAuthManagerInputProtocol: ApiDataManagerInputProtocol {}

protocol LoginModuleAuthManagerOutputProtocol: ApiDataManagerOutputProtocol {}

class LoginAuthManager: ApiDataManager,
LoginModuleAuthManagerInputProtocol {}

protocol LoginAuthManagerInputProtocol: LoginModuleAuthManagerInputProtocol{
  
  func verification(email: String, password: String)
  func checkUserExistence()
  func googleSigning(idToken: String, accessToken: String)
}

protocol LoginAuthManagerOutputProtocol: LoginModuleAuthManagerOutputProtocol {
  
  func success(response: String)
  func failure(response: String)
  func userFound(response: String)
  func googleSignInSuccess(uid: String)
}

extension LoginAuthManager: LoginAuthManagerInputProtocol {
  
  func googleSigning(idToken: String, accessToken: String) {
    let credentials = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
    
    Auth.auth().signIn(with: credentials) { (user, error) in
      if let error = error {
        print("Failed to create Firebase user with Google Account", error)
      }
      
      guard let uid = user?.user.uid else { return }
      print("Successfully Logged into Firebase",uid)
      self.output?.googleSignInSuccess(uid: uid)
    }
  }
  
  func checkUserExistence() {
    if Auth.auth().currentUser?.uid != nil {
      self.output?.userFound(response: Auth.auth().currentUser?.uid ?? "")
    } 
  }
  
  var output: LoginAuthManagerOutputProtocol?{
    return _output as? LoginAuthManagerOutputProtocol
  }
  
  func verification(email: String, password: String) {
    
    Auth.auth().signIn(withEmail: email , password: password ) { (result, error) in
      
      if error != nil {
        self.output?.failure(response: error?.localizedDescription ?? "")
        
      } else {
        self.output?.success(response: result?.user.uid ?? "")
      }
    }
  }
}
