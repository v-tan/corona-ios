//
//  IndiaChartViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 25/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit
import Charts

class IndiaChartViewController: BaseViewController, IndiaChartViewProtocol , ChartViewDelegate {
  
  @IBOutlet weak var firstRangeTextField: UITextField!
  @IBOutlet weak var lastRangeTextField: UITextField!
  @IBOutlet var lineChart: LineChartView!
  @IBOutlet weak var lineChartView: LineChartView!
  
  let months = ["January","Febrary","March","April","May","June","July","August","September","October","November","December"]
  
  var variable = ["Daily", "Monthly", "Weekly"]
  
  private var datePicker: UIDatePicker?
  
  var finalDate : String?
  var lastDate: String?
  
  var totalJanDeathCases = 0
  var totalJanCureCases = 0
  var totalJanConfirmedCases = 0
  
  var totalFebDeathCases = 0
  var totalFebCureCases = 0
  var totalFebConfirmedCases = 0
  
  var totalMarchDeathCases = 0
  var totalMarchCureCases = 0
  var totalMarchConfirmedCases = 0
  
  var totalAprilDeathCases = 0
  var totalAprilCureCases = 0
  var totalAprilConfirmedCases = 0
  
  var totalMayDeathCases = 0
  var totalMayCureCases = 0
  var totalMayConfirmedCases = 0
  
  var totalJuneDeathCases = 0
  var totalJuneCureCases = 0
  var totalJuneConfirmedCases = 0
  
  var totalJulyDeathCases = 0
  var totalJulyCureCases = 0
  var totalJulyConfirmedCases = 0
  
  var totalAugustDeathCases = 0
  var totalAugustCureCases = 0
  var totalAugustConfirmedCases = 0
  
  var totalSeptemberDeathCases = 0
  var totalSeptemberCureCases = 0
  var totalSeptemberConfirmedCases = 0
  
  var dataModel : CityDataModel?
  
  var weekValues: [Int] = []
  var dailyValues: [Int] = []
  
  var julyDeathArray: [Int] = []
  var julyCureArray: [Int] = []
  var julyTestArray: [Int] = []
  
  var janDeathArray: [Int] = []
  var janCureArray: [Int] = []
  var janTestArray: [Int] = []
  
  var febDeathArray: [Int] = []
  var febCureArray: [Int] = []
  var febTestArray: [Int] = []
  
  var marchDeathArray: [Int] = []
  var marchCureArray: [Int] = []
  var marchTestArray: [Int] = []
  
  var aprilDeathArray: [Int] = []
  var aprilCureArray: [Int] = []
  var aprilTestArray: [Int] = []
  
  var mayDeathArray: [Int] = []
  var mayCureArray: [Int] = []
  var mayTestArray: [Int] = []
  
  var juneDeathArray: [Int] = []
  var juneCureArray: [Int] = []
  var juneTestArray: [Int] = []
  
  var augDeathArray: [Int] = []
  var augCureArray: [Int] = []
  var augTestArray: [Int] = []
  
  var septDeathArray: [Int] = []
  var septCureArray: [Int] = []
  var septTestArray: [Int] = []
  
  //Daily
  var dailyDeathValues: [String] = []
  var dailyCureValues: [String] = []
  var dailyConfirmedValues: [String] = []
  
  //Weekly
  var weekDeathCases: [String] = []
  var weekCureCases: [String] = []
  var weekConfirmedCases: [String] = []
  
  //Monthly
  var setDeathArray: [Int] = []
  var setCureArray: [Int] = []
  var setConfirmedArray: [Int] = []
  
  var total1 = 0
  var total = 0
  
  var valueFormatter: [String] = []
  
  let data = LineChartData()
  let data1 = LineChartData()
  let data2 = LineChartData()
  let data3 = LineChartData()
  
  var lineChartEntry2 = [ChartDataEntry]()
  var lineChartEntry3 = [ChartDataEntry]()
  var lineChartEntry1 = [ChartDataEntry]()
  
  //Daily
  var lineChartEntry4 = [ChartDataEntry]()
  var lineChartEntry9 = [ChartDataEntry]()
  var lineChartEntry10 = [ChartDataEntry]()
  
  //Weekly
  var lineChartEntry5 = [ChartDataEntry]()
  var lineChartEntry11 = [ChartDataEntry]()
  var lineChartEntry12 = [ChartDataEntry]()
  
  //Month
  var lineChartEntry6 = [ChartDataEntry]()
  var lineChartEntry7 = [ChartDataEntry]()
  var lineChartEntry8 = [ChartDataEntry]()
  
  var totalDeathCases: [String] = []
  var totalRecoveredCases: [String] = []
  var totalConfirmedCases: [String] = []
  var newArray : [IChartDataSet] = []
  var dateArray: [IChartDataSet] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    backButton()
    lineChart.delegate = self
    lineChartView.delegate = self
    
    datePicker = UIDatePicker()
    datePicker?.datePickerMode = .date
    datePicker?.addTarget(self, action: #selector(IndiaChartViewController.dateChanged(datePicker:)), for: .valueChanged)
    
    firstRangeTextField.inputView = datePicker
    lastRangeTextField.inputView = datePicker
    
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IndiaChartViewController.viewTapped(gestureRecognizer:))
    )
    
    view.addGestureRecognizer(tapGesture)
    
    guard let url = URL(string: "https://api.covid19india.org/data.json" ) else { return }
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      
      do {
        if error == nil {
          let indiaData = try JSONDecoder().decode(CityDataModel.self, from: data!)
          
          for i in 0..<indiaData.casesTimeSeries.count {
            self.totalDeathCases.append(indiaData.casesTimeSeries[i].totaldeceased)
            self.totalConfirmedCases.append(indiaData.casesTimeSeries[i].totalconfirmed)
            self.totalRecoveredCases.append(indiaData.casesTimeSeries[i].totalrecovered)
            self.valueFormatter.append(indiaData.casesTimeSeries[i].date)
            self.dataModel = indiaData
          }
        }
        DispatchQueue.main.async {
          
          for i in 0..<self.valueFormatter.count {
            
            let data =  self.valueFormatter[i].components(separatedBy: " ")
            if data[1] == "August"{
              self.augDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.augCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.augTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            
            if data[1] == "January" {
              self.janDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.janCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.janTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            if data[1] == "March" {
              self.marchDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.marchCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.marchTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            if data[1] == "April" {
              self.aprilDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.aprilCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.aprilTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            if data[1] == "February" {
              self.febDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.febCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.febTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            if data[1] == "May" {
              self.mayDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.mayCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.mayTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            
            if data[1] == "June" {
              self.juneDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.juneCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.juneTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            if data[1] == "July" {
              self.julyDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.julyCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.julyTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
            
            if data[1] == "September" {
              self.septDeathArray.append(Int(self.totalDeathCases[i]) ?? 0)
              self.septCureArray.append(Int(self.totalRecoveredCases[i]) ?? 0)
              self.septTestArray.append(Int(self.totalConfirmedCases[i]) ?? 0)
            }
          }
          
          self.setUp()
        }
      } catch {
        print("Error Found")
      }
    }.resume()
  }
  
  func setUp() {
    
    for i in 0..<totalDeathCases.count {
      lineChartEntry1.append(ChartDataEntry(x:Double(i), y:  Double(Int(totalDeathCases[i]) ?? 0)))
    }
    
    let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Death Dataset")
    data.addDataSet(line1)
    
    line1.colors = [UIColor.red]
    line1.drawValuesEnabled = false
    line1.drawCirclesEnabled = false
    
    if (totalDeathCases.count > 0) {
      
      for i in 0..<totalDeathCases.count {
        lineChartEntry2.append(ChartDataEntry(x:Double(i), y:  Double(Int(totalRecoveredCases[i]) ?? 0)))
      }
      
      let line2 = LineChartDataSet(entries: lineChartEntry2, label: "Recovered Dataset")
      data.addDataSet(line2)
      
      line2.colors = [UIColor.green]
      line2.drawValuesEnabled = false
      line2.drawCirclesEnabled = false
    }
    
    if (totalDeathCases.count > 0) {
      
      for i in 0..<totalDeathCases.count {
        lineChartEntry3.append(ChartDataEntry(x:Double(i), y:  Double(Int(totalConfirmedCases[i]) ?? 0)))
      }
      
      let line3 = LineChartDataSet(entries: lineChartEntry3, label: "Cases Dataset")
      data.addDataSet(line3)
      
      line3.colors = [UIColor.blue]
      line3.drawValuesEnabled = false
      line3.drawCirclesEnabled = false
    }
    
    self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: valueFormatter)
    self.lineChart.data = data
    
    data.setDrawValues(true)
    lineChart.xAxis.granularity = 1
    lineChart.animate(xAxisDuration: 2.5)
    lineChart.xAxis.labelPosition = .bottom
    lineChart.xAxis.drawGridLinesEnabled = false
    lineChart.chartDescription?.enabled = false
    lineChart.legend.enabled = true
    lineChart.rightAxis.enabled = false
    lineChart.leftAxis.drawGridLinesEnabled = false
    lineChart.leftAxis.drawLabelsEnabled = true
  }
  
  @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
    view.endEditing(true)
  }
  
  @objc func dateChanged(datePicker: UIDatePicker) {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    if firstRangeTextField.isEditing == true {
      firstRangeTextField.text = dateFormatter.string(from: datePicker.date)
    } else {
      lastRangeTextField.text = dateFormatter.string(from: datePicker.date)
    }
    view.endEditing(true)
  }
  
  func backButton() {
    self.navigationController?.isNavigationBarHidden = false
    let logOutButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    logOutButton.setTitle("Back", for: .normal)
    logOutButton.setTitleColor(UIColor.red, for: .normal)
    logOutButton.addTarget(self, action: #selector(back), for: UIControl.Event.touchUpInside)
    logOutButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    
    let logOutbarButton = UIBarButtonItem(customView: logOutButton)
    self.navigationItem.leftBarButtonItem = logOutbarButton
    
  }
  
  @objc func back() {
    navigationController?.popToRootViewController(animated: true)
  }
  
  @IBAction func deathButtonClicked(_ sender: UIButton) {
    
    if sender.isSelected {
      sender.isSelected = false
      
      newArray = []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        
        if "Death Dataset" == (newArray[index].label) {
          data.removeDataSetByIndex(index)
        }
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: valueFormatter )
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      for i in 0..<totalDeathCases.count {
        lineChartEntry1.append(ChartDataEntry(x:Double(i), y:  Double(totalDeathCases[i])!))
      }
      
      let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Death Dataset")
      data.addDataSet(line1)
      
      line1.colors = [UIColor.red]
      line1.drawValuesEnabled = false
      line1.drawCirclesEnabled = false
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: valueFormatter)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
    }
  }
  
  @IBAction func casesButtonClicked(_ sender: UIButton) {
    if sender.isSelected {
      
      sender.isSelected = false
      
      newArray =  []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        
        if "Cases Dataset" == (newArray[index].label) {
          data.removeDataSetByIndex(index)
        }
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: valueFormatter)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
      
    } else {
      sender.isSelected = true
      
      if (totalDeathCases.count > 0) {
        
        for i in 0..<totalDeathCases.count {
          lineChartEntry3.append(ChartDataEntry(x:Double(i), y:  Double(totalConfirmedCases[i])!))
        }
        
        let line3 = LineChartDataSet(entries: lineChartEntry3, label: "Cases Dataset")
        data.addDataSet(line3)
        
        line3.colors = [UIColor.blue]
        line3.drawValuesEnabled = false
        line3.drawCirclesEnabled = false
        
        self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: valueFormatter)
        self.lineChart.data = data
        
        data.setDrawValues(true)
        lineChart.xAxis.granularity = 1
        lineChart.animate(xAxisDuration: 2.5)
        lineChart.xAxis.labelPosition = .bottom
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.chartDescription?.enabled = false
        lineChart.legend.enabled = true
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawLabelsEnabled = true
      }
    }
  }
  
  @IBAction func cureButtonClicked(_ sender: UIButton) {
    if sender.isSelected {
      
      sender.isSelected = false
      newArray = []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        
        if "Recovered Dataset" == (newArray[index].label) {
          data.removeDataSetByIndex(index)
        }
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: valueFormatter)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      
      if (totalDeathCases.count > 0) {
        
        for i in 0..<totalDeathCases.count {
          lineChartEntry2.append(ChartDataEntry(x:Double(i), y:  Double(totalRecoveredCases[i])!))
        }
        
        let line2 = LineChartDataSet(entries: lineChartEntry2, label: "Recovered Dataset")
        data.addDataSet(line2)
        
        line2.colors = [UIColor.green]
        line2.drawValuesEnabled = false
        line2.drawCirclesEnabled = false
        self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: valueFormatter)
        self.lineChart.data = data
        data.setDrawValues(true)
        lineChart.xAxis.granularity = 1
        lineChart.animate(xAxisDuration: 2.5)
        lineChart.xAxis.labelPosition = .bottom
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.chartDescription?.enabled = false
        lineChart.legend.enabled = true
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawLabelsEnabled = true
      }
    }
  }
}

extension IndiaChartViewController: UIPickerViewDataSource, UIPickerViewDelegate {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    variable.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return variable[row]
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    var deathDataSet: [Int] = []
    var confirmedDataSet: [Int] = []
    var cureDataSet: [Int] = []
    
    totalJanDeathCases = (janDeathArray.last ?? 0) - janDeathArray[0]
    totalJanCureCases = (janCureArray.last ?? 0) - janCureArray[0]
    totalJanConfirmedCases = (janTestArray.last ?? 0) - janTestArray[0]
    
    deathDataSet.append(totalJanDeathCases)
    confirmedDataSet.append(totalJanConfirmedCases)
    cureDataSet.append(totalJanCureCases)
    
    
    totalFebDeathCases = (febDeathArray.last ?? 0) - febDeathArray[0]
    totalFebCureCases = (febCureArray.last ?? 0) - febCureArray[0]
    totalFebConfirmedCases = (febTestArray.last ?? 0) - febTestArray[0]
    
    deathDataSet.append(totalFebDeathCases)
    confirmedDataSet.append(totalJanConfirmedCases)
    cureDataSet.append(totalFebCureCases)
    
    totalMarchDeathCases = (marchDeathArray.last ?? 0) - marchDeathArray[0]
    totalMarchCureCases = (marchCureArray.last ?? 0) - marchCureArray[0]
    totalMarchConfirmedCases = (marchTestArray.last ?? 0) - marchTestArray[0]
    
    deathDataSet.append(totalMarchDeathCases)
    confirmedDataSet.append(totalMarchConfirmedCases)
    cureDataSet.append(totalMarchCureCases)
    
    totalAprilDeathCases = (aprilDeathArray.last ?? 0) - aprilDeathArray[0]
    totalAprilCureCases = (aprilCureArray.last ?? 0) - aprilCureArray[0]
    totalAprilConfirmedCases = (aprilTestArray.last ?? 0) - aprilTestArray[0]
    
    
    deathDataSet.append(totalAprilDeathCases)
    confirmedDataSet.append(totalAprilConfirmedCases)
    cureDataSet.append(totalAprilCureCases)
    
    totalMayDeathCases = (mayDeathArray.last ?? 0) - mayDeathArray[0]
    totalMayCureCases = (mayCureArray.last ?? 0) - mayCureArray[0]
    totalMayConfirmedCases = (mayTestArray.last ?? 0) - mayTestArray[0]
    
    deathDataSet.append(totalMayDeathCases)
    confirmedDataSet.append(totalMayConfirmedCases)
    cureDataSet.append(totalMayCureCases)
    
    totalJuneDeathCases = (juneDeathArray.last ?? 0) - juneDeathArray[0]
    totalJuneCureCases = (juneCureArray.last ?? 0) - juneCureArray[0]
    totalJuneConfirmedCases = (juneTestArray.last ?? 0) - juneTestArray[0]
    
    deathDataSet.append(totalJuneDeathCases)
    confirmedDataSet.append(totalJuneConfirmedCases)
    cureDataSet.append(totalJuneCureCases)
    
    totalJulyDeathCases = (julyDeathArray.last ?? 0) - julyDeathArray[0]
    totalJulyCureCases = (julyCureArray.last ?? 0) - julyCureArray[0]
    totalJulyConfirmedCases = (julyTestArray.last ?? 0) - julyTestArray[0]
    
    deathDataSet.append(totalJulyDeathCases)
    confirmedDataSet.append(totalJulyConfirmedCases)
    cureDataSet.append(totalJulyCureCases)
    
    totalAugustDeathCases = (augDeathArray.last ?? 0) - augDeathArray[0]
    totalAugustCureCases = (augCureArray.last ?? 0) - augCureArray[0]
    totalAugustConfirmedCases = (augTestArray.last ?? 0) - augTestArray[0]
    
    deathDataSet.append(totalAugustDeathCases)
    confirmedDataSet.append(totalAugustConfirmedCases)
    cureDataSet.append(totalAugustCureCases)
    
    totalSeptemberDeathCases = (septDeathArray.last ?? 0) - septDeathArray[0]
    totalSeptemberCureCases = (septCureArray.last ?? 0) - septCureArray[0]
    totalSeptemberConfirmedCases = (septTestArray.last ?? 0) - septTestArray[0]
    
    deathDataSet.append(totalSeptemberDeathCases)
    confirmedDataSet.append(totalSeptemberConfirmedCases)
    cureDataSet.append(totalSeptemberCureCases)
    
    
    func dateSplitter(date: String) -> String {
      
      let components = date.components(separatedBy: "/")
      let month = components[0]
      
      if month == "01" {
        finalDate = components[1] + " " + "January" + " "
        return finalDate ?? ""
        
      }else if (month == "02") {
        finalDate = components[1] + " " + "February" + " "
        return finalDate ?? ""
        
      } else if (month == "03") {
        finalDate = components[1] + " " + "March" + " "
        return finalDate ?? ""
        
      } else if (month == "04") {
        finalDate = components[1] + " " + "April" + " "
        return finalDate ?? ""
        
      } else if (month == "05") {
        finalDate = components[1] + " " + "May" + " "
        return finalDate ?? ""
        
      } else if (month == "06") {
        finalDate = components[1] + " " + "June" + " "
        return finalDate ?? ""
        
      } else if (month == "07") {
        finalDate = components[1] + " " + "July" + " "
        return finalDate ?? ""
        
      } else if (month == "08") {
        finalDate = components[1] + " " + "August" + " "
        return finalDate ?? ""
        
      } else if (month == "09") {
        finalDate = components[1] + " " + "September" + " "
        return finalDate ?? ""
        
      } else {
        return ""
      }
    }
    
    var emptySet = Set<String>()
    
    if variable[row] == "Daily" {
      data1.clearValues()
      dailyDeathValues.removeAll()
      dailyCureValues.removeAll()
      dailyConfirmedValues.removeAll()
      dailyValues.removeAll()
      
      for i in 0..<self.valueFormatter.count {
        
        let date1 = dateSplitter(date: firstRangeTextField.text ?? "")
        let date2 = dateSplitter(date: lastRangeTextField.text ?? "")
        
        if dataModel?.casesTimeSeries[i].date == date1 {
          
          emptySet.insert(self.valueFormatter[i])
          
          for k in i..<self.valueFormatter.count {
            
            if dataModel?.casesTimeSeries[k].date == date2{
              
              emptySet.insert(self.valueFormatter[k])
              break
              
            } else {
              emptySet.insert(self.valueFormatter[k])
            }
          }
        }
      }
      
      var dateData : [String] = []
      var index: [Int] = []
      
      for value in valueFormatter {
        
        for setValue in emptySet {
          if value == setValue {
            dateData.append(value)
          }
        }
      }
      
      for value in valueFormatter {
        for date in dateData {
          
          if value == date {
            index.append(valueFormatter.firstIndex(of: value) ?? 0)
          }
        }
      }
      
      for indexValue in index {
        dailyValues.append(indexValue)
        dailyDeathValues.append(totalDeathCases[Int(indexValue)])
        dailyCureValues.append(totalRecoveredCases[Int(indexValue)])
        dailyConfirmedValues.append(totalConfirmedCases[Int(indexValue)])
        
      }
      
      for i in 0..<dailyValues.count {
        lineChartEntry4.append(ChartDataEntry(x:Double(i), y: Double(Int(dailyDeathValues[i]) ?? Int(0.0))))
      }
      
      let line4 = LineChartDataSet(entries: lineChartEntry4, label: "Daily DeathDataset")
      data1.addDataSet(line4)
      
      line4.colors = [UIColor.red]
      line4.drawValuesEnabled = false
      line4.drawCirclesEnabled = false
      
      for i in 0..<dailyValues.count {
        lineChartEntry9.append(ChartDataEntry(x:Double(i), y: Double(Int(dailyCureValues[i]) ?? Int(0.0))))
      }
      
      let line9 = LineChartDataSet(entries: lineChartEntry9, label: "Daily CureDataset")
      data1.addDataSet(line9)
      
      line9.colors = [UIColor.green]
      line9.drawValuesEnabled = false
      line9.drawCirclesEnabled = false
      
      for i in 0..<dailyValues.count {
        lineChartEntry10.append(ChartDataEntry(x:Double(i), y: Double(Int(dailyConfirmedValues[i]) ?? Int(0.0))))
      }
      
      let line10 = LineChartDataSet(entries: lineChartEntry10, label: "Daily ConfirmedDataset")
      data1.addDataSet(line10)
      
      line10.colors = [UIColor.blue]
      line10.drawValuesEnabled = false
      line10.drawCirclesEnabled = false
      
      self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: dateData)
      self.lineChartView.data = data1
      
      data1.setDrawValues(true)
      lineChartView.xAxis.granularity = 1
      lineChartView.animate(xAxisDuration: 2.5)
      lineChartView.xAxis.labelPosition = .bottom
      lineChartView.xAxis.drawGridLinesEnabled = false
      lineChartView.chartDescription?.enabled = false
      lineChartView.legend.enabled = true
      lineChartView.rightAxis.enabled = false
      lineChartView.leftAxis.drawGridLinesEnabled = false
      lineChartView.leftAxis.drawLabelsEnabled = true
      lineChartView.leftAxis.resetCustomAxisMin()
      lineChartView.leftAxis.resetCustomAxisMax()
      lineChartView.leftAxis.drawZeroLineEnabled = false
      
      let max =  Double(weekValues.max() ?? 0)
      let min =  Double(weekValues.min() ?? 0)
      
      let defaults = UserDefaults.standard
      defaults.set(max ,forKey: "max")
      defaults.set(min ,forKey: "min")
      
      let leftAxis = lineChartView.leftAxis
      leftAxis.labelCount = 2
      leftAxis.granularity = Double(dateData.count)
      leftAxis.valueFormatter = MyAxisValueFormatter()
      
    } else if variable[row] == "Weekly" {
      
      data2.clearValues()
      weekDeathCases.removeAll()
      weekConfirmedCases.removeAll()
      weekCureCases.removeAll()
      
      for i in 0..<self.valueFormatter.count {
        let date1 = dateSplitter(date: firstRangeTextField.text ?? "")
        let date2 = dateSplitter(date: lastRangeTextField.text ?? "")
        
        if dataModel?.casesTimeSeries[i].date == date1 {
          
          emptySet.insert(self.valueFormatter[i])
          
          for k in i..<self.valueFormatter.count {
            
            if dataModel?.casesTimeSeries[k].date == date2 {
              
              emptySet.insert(self.valueFormatter[k])
              break
              
            } else {
              emptySet.insert(self.valueFormatter[k])
              
            }
          }
        }
      }
      
      var dateData : [String] = []
      var index: [Int] = []
      
      for value in valueFormatter {
        for setValue in emptySet {
          if value == setValue {
            dateData.append(value)
          }
        }
      }
      
      for value in valueFormatter {
        for date in dateData {
          
          if value == date {
            index.append(valueFormatter.firstIndex(of: value) ?? 0)
          }
        }
      }
      
      for indexValue in index {
        weekValues.append(indexValue)
      }
      
      for q in 0..<dateData.count {
        
        if q%6 == 0 {
          print(dateData[q])
          print(weekValues[q])
        }
      }
      
      var sortedDated: [String] = []
      let dates = Array(dateData)
      let result = dates.chunked(into: 6)
      
      for response in 0..<result.count {
        print(result[response][0])
        sortedDated.append(result[response][0])
      }
      
      for value in valueFormatter {
        for date in sortedDated {
          
          if value == date {
            weekDeathCases.append(totalDeathCases[valueFormatter.firstIndex(of: value) ?? 0])
            weekCureCases.append(totalRecoveredCases[valueFormatter.firstIndex(of: value) ?? 0])
            weekConfirmedCases.append(totalConfirmedCases[valueFormatter.firstIndex(of: value) ?? 0])
          }
        }
      }
      
      for i in 0..<weekDeathCases.count {
        lineChartEntry5.append(ChartDataEntry(x:Double(i), y: Double(Int(weekDeathCases[i]) ?? Int(0.0))))
      }
      
      let line5 = LineChartDataSet(entries: lineChartEntry5, label: "Weekly DeathDataset")
      data2.addDataSet(line5)
      line5.colors = [UIColor.red]
      line5.drawValuesEnabled = false
      line5.drawCirclesEnabled = false
      
      for i in 0..<weekDeathCases.count {
        lineChartEntry11.append(ChartDataEntry(x:Double(i), y: Double(Int(weekCureCases[i]) ?? Int(0.0))))
      }
      
      let line11 = LineChartDataSet(entries: lineChartEntry11, label: "Weekly CureDataset")
      data2.addDataSet(line11)
      line11.colors = [UIColor.green]
      line11.drawValuesEnabled = false
      line11.drawCirclesEnabled = false
      
      for i in 0..<weekDeathCases.count {
        lineChartEntry12.append(ChartDataEntry(x:Double(i), y: Double(Int(weekConfirmedCases[i]) ?? Int(0.0))))
      }
      
      let line12 = LineChartDataSet(entries: lineChartEntry12, label: "Weekly ConfirmedDataset")
      
      data2.addDataSet(line12)
      line12.colors = [UIColor.blue]
      line12.drawValuesEnabled = false
      line12.drawCirclesEnabled = false
      
      self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: sortedDated)
      self.lineChartView.data = data2
      
      data2.setDrawValues(true)
      lineChartView.xAxis.granularity = 1
      lineChartView.animate(xAxisDuration: 2.5)
      lineChartView.xAxis.labelPosition = .bottom
      lineChartView.xAxis.drawGridLinesEnabled = false
      lineChartView.chartDescription?.enabled = false
      lineChartView.legend.enabled = true
      lineChartView.rightAxis.enabled = false
      lineChartView.leftAxis.drawGridLinesEnabled = false
      lineChartView.leftAxis.drawLabelsEnabled = true
      lineChartView.leftAxis.resetCustomAxisMin()
      lineChartView.leftAxis.resetCustomAxisMax()
      lineChartView.leftAxis.drawZeroLineEnabled = false
      
      let intArray = weekDeathCases.map({Int($0) ?? 0})
      
      let max =  Double(intArray.max() ?? 0)
      let min =  Double(intArray.min() ?? 0)
      
      let defaults = UserDefaults.standard
      defaults.set(max ,forKey: "max")
      defaults.set(min ,forKey: "min")
      
      let leftAxis = lineChartView.leftAxis
      leftAxis.labelCount = 2
      leftAxis.granularity = Double(sortedDated.count)
      leftAxis.valueFormatter = MyAxisValueFormatter()
    }
      
    else if variable[row] == "Monthly" {
      data3.clearValues()
      setConfirmedArray.removeAll()
      setCureArray.removeAll()
      setDeathArray.removeAll()
      
      for i in 0..<self.valueFormatter.count {
        let date1 = dateSplitter(date: firstRangeTextField.text ?? "")
        let date2 = dateSplitter(date: lastRangeTextField.text ?? "")
        
        if dataModel?.casesTimeSeries[i].date == date1 {
          
          let data =  self.valueFormatter[i].components(separatedBy: " ")
          emptySet.insert(data[1])
          
          for k in i..<self.valueFormatter.count {
            
            if dataModel?.casesTimeSeries[k].date == date2 {
              
              let data =  self.valueFormatter[k].components(separatedBy: " ")
              emptySet.insert(data[1])
              break
              
            } else {
              
              let data =  self.valueFormatter[k].components(separatedBy: " ")
              emptySet.insert(data[1])
            }
          }
        }
      }
      
      for month in months {
        for value in emptySet {
          if month == value {
            setDeathArray.append(deathDataSet[months.firstIndex(of: month) ?? 0])
            setCureArray.append(cureDataSet[months.firstIndex(of: month) ?? 0])
            setConfirmedArray.append(confirmedDataSet[months.firstIndex(of: month) ?? 0])
          }
        }
      }
      
      for i in 0..<emptySet.count {
        lineChartEntry6.append(ChartDataEntry(x:Double(i), y: Double(setDeathArray[i])))
      }
      
      for i in 0..<emptySet.count {
        lineChartEntry7.append(ChartDataEntry(x:Double(i), y: Double(setCureArray[i])))
      }
      
      for i in 0..<emptySet.count {
        lineChartEntry8.append(ChartDataEntry(x:Double(i), y: Double(setConfirmedArray[i])))
      }
      
      let line6 = LineChartDataSet(entries: lineChartEntry6, label: "Monthly DeathDataset")
      let line7 = LineChartDataSet(entries: lineChartEntry7, label: "Monthly CureDataset")
      let line8 = LineChartDataSet(entries: lineChartEntry8, label: "Monthly ConfirmedDataset")
      
      data3.addDataSet(line6)
      data3.addDataSet(line7)
      data3.addDataSet(line8)
      
      line6.colors = [UIColor.red]
      line6.drawValuesEnabled = false
      line6.drawCirclesEnabled = false
      
      line7.colors = [UIColor.green]
      line7.drawValuesEnabled = false
      line7.drawCirclesEnabled = false
      
      line8.colors = [UIColor.blue]
      line8.drawValuesEnabled = false
      line8.drawCirclesEnabled = false
      
      let xAxisValueFormatter = Array(emptySet)
      var newArray : [String] = []
      
      for month in months {
        for value in xAxisValueFormatter {
          
          if month == value {
            newArray.append(month)
          }
        }
      }
      
      self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: newArray)
      self.lineChartView.data = data3
      
      data3.setDrawValues(true)
      lineChartView.xAxis.granularity = 1
      lineChartView.animate(xAxisDuration: 2.5)
      lineChartView.xAxis.labelPosition = .bottom
      lineChartView.xAxis.drawGridLinesEnabled = false
      lineChartView.chartDescription?.enabled = false
      lineChartView.legend.enabled = true
      lineChartView.rightAxis.enabled = false
      lineChartView.leftAxis.drawGridLinesEnabled = false
      lineChartView.leftAxis.drawLabelsEnabled = true
      lineChartView.leftAxis.resetCustomAxisMin()
      lineChartView.leftAxis.resetCustomAxisMax()
      lineChartView.leftAxis.drawZeroLineEnabled = false
      
      let max =  Double(setDeathArray.max() ?? 0)
      let min =  Double(setDeathArray.min() ?? 0)
      
      let defaults = UserDefaults.standard
      defaults.set(max ,forKey: "max")
      defaults.set(min ,forKey: "min")
      
      let leftAxis = lineChartView.leftAxis
      leftAxis.labelCount = 2
      leftAxis.granularity = Double(self.newArray.count)
      leftAxis.valueFormatter = MyAxisValueFormatter()
    }
  }
}

extension Array {
  func chunked(into size: Int) -> [[Element]] {
    return stride(from: 0, to: count, by: size).map {
      Array(self[$0 ..< Swift.min($0 + size, count)])
    }
  }
}
