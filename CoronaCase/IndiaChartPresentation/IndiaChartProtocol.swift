//
//  IndiaChartProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 25/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol  IndiaChartViewProtocol: ViperViewProtocol {}

extension IndiaChartViewProtocol {
  var presenter: IndiaChartPresenterProtocol? {
    return _presenter as? IndiaChartPresenterProtocol
  }
}

protocol IndiaChartPresenterProtocol: ViperBasePresenterProtocol {}

extension IndiaChartPresenterProtocol {
  var view: IndiaChartViewProtocol? {
    return _view as? IndiaChartViewProtocol
  }
  
  var interactor: IndiaChartInteractorInputProtocol? {
    _interactor as? IndiaChartInteractorInputProtocol
  }
  
  var wireframe: IndiaChartWireframeProtocol? {
    return _wireframe as? IndiaChartWireframeProtocol
  }
}

protocol IndiaChartInteractorInputProtocol: ViperBaseInteractorInputProtocol {}

extension IndiaChartInteractorInputProtocol {
  var output: IndiaChartInteractorOutputProtocol? {
    return _output as? IndiaChartInteractorOutputProtocol
  }
}

protocol IndiaChartInteractorOutputProtocol: ViperBaseInteractorOutputProtocol {}

protocol IndiaChartWireframeProtocol: ViperBaseWireframeProtocol {}



