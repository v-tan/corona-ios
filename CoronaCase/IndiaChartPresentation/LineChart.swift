//
//  LineChart.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 25/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation
import Charts

class MyAxisValueFormatter : IAxisValueFormatter {
  
  static public var instance = MyAxisValueFormatter()
  
  func stringForValue(_ value: Double, axis: AxisBase?) -> String {
    let intVal = Int(value)
    
    let max = UserDefaults.standard.double(forKey: "max")
    let min = UserDefaults.standard.double(forKey: "min")
    
    if intVal == Int(max) {
      return "Max Cases"
    } else if intVal == Int(min) {
      return "Min Cases"
    } else {
      return "\(intVal)"
    }
  }
}
