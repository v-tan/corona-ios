//
//  IndiaChartWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 25/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class IndiaChartWireframe: ViperBaseWireframe, IndiaChartWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: IndiaChartViewController.self,
      presenter: IndiaChartPresenter.self,
      interactor: nil,
      apiManager: nil, localManager: nil)
    return model
  }
}
