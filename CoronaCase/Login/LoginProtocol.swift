//
//  LoginProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol LoginViewProtocol: ViperViewProtocol {
  func errorText(error: String)
}

extension LoginViewProtocol {
  var presenter: LoginPresenterProtocol? {
    return _presenter as? LoginPresenterProtocol
  }
}

protocol LoginPresenterProtocol: ViperBasePresenterProtocol {
  func signUpVc()
  func authenticationCheck(email: String, password: String)
  func checkUserExists()
  func googleSignIn(idToken: String, accessToken: String)
}

extension LoginPresenterProtocol {
  var view: LoginViewProtocol? {
    return _view as? LoginViewProtocol
  }
  
  var interactor: LoginInteractorInputProtocol? {
    _interactor as? LoginInteractorInputProtocol
  }
  
  var wireframe: LoginWireframeProtocol? {
    return _wireframe as? LoginWireframeProtocol
  }
}

protocol LoginInteractorInputProtocol: ViperBaseInteractorInputProtocol {
  func authCheck(email: String, password: String)
  func checkUser()
  func googleAuth(idToken: String, accessToken: String)
  
}

extension LoginInteractorInputProtocol {
  var output: LoginInteractorOutputProtocol? {
    return _output as? LoginInteractorOutputProtocol
  }
  var apiDataManager: LoginAuthManagerInputProtocol? {
    return _apiDataManager as? LoginAuthManagerInputProtocol
  }
}

protocol LoginInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, LoginAuthManagerOutputProtocol  {}

protocol LoginWireframeProtocol: ViperBaseWireframeProtocol {
  func signUpViewController(view: LoginViewProtocol)
  func countriesViewController(view: LoginViewProtocol, uid: String)
}
