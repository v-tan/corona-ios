//
//  LoginViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseAuth
import AVKit

class LoginViewController: BaseViewController, LoginViewProtocol {
  
  @IBOutlet var emailTextField: UITextField!
  @IBOutlet weak var googleSignInView: UIView!
  @IBOutlet weak var googleLoginButton: UIButton!
  @IBOutlet var passwordTextField: UITextField!
  @IBOutlet var loginButton: UIButton!
  @IBOutlet var errorLabel: UILabel!
  @IBOutlet var signUpButton: UIButton!
  
  var videoPlayer: AVPlayer?
  var videoPlayerLayer: AVPlayerLayer?
  
  var isPresent: Bool?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    initialSetUp()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
 
    setupVideo()
    self.presenter?.checkUserExists()
    self.navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  func initialSetUp() {
    googleSignInView.layer.masksToBounds = true
    loginButton.layer.masksToBounds = true
    loginButton.layer.cornerRadius = self.view.frame.height/32
    googleSignInView.layer.cornerRadius = self.view.frame.height/32
    loginButton.layer.borderWidth = 1
    loginButton.layer.borderColor = UIColor.black.cgColor
    googleSignInView.layer.borderWidth = 1
    googleSignInView.layer.borderColor = UIColor.black.cgColor
  }
  
  func setupVideo() {
    let bundlePath = Bundle.main.path(forResource: "video", ofType: "mp4")
    
    guard bundlePath != nil else {
      return
    }
    
    let url = URL(fileURLWithPath: bundlePath!)
    let item = AVPlayerItem(url: url)
    
    videoPlayer = AVPlayer(playerItem: item)
    videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
    videoPlayerLayer?.frame = self.view.frame
    videoPlayerLayer?.videoGravity = .resizeAspectFill
    view.layer.insertSublayer(videoPlayerLayer!, at: 0)
    videoPlayer?.playImmediately(atRate: 0.8)
  }
  
  func googleSignInSetup() {
    GIDSignIn.sharedInstance().delegate = self
    GIDSignIn.sharedInstance()?.presentingViewController = self
    let googleSignInButton = GIDSignInButton()
    self.googleSignInView.addSubview(googleSignInButton)
  }
  
  func errorText(error: String) {
    errorLabel.text = error
  }
  
  @IBAction func googleLoginClicked(_ sender: UIButton) {
    GIDSignIn.sharedInstance().delegate = self
    GIDSignIn.sharedInstance()?.presentingViewController = self
    GIDSignIn.sharedInstance()?.signIn()
  }
  
  @IBAction func signUpButtonClicked(_ sender: UIButton) {
    self.presenter?.signUpVc()
  }
  
  @IBAction func loginButtonTapped(_ sender: UIButton) {
    
    if !Validation.isValidEmail(string: emailTextField.text) {
      errorLabel.text = "Incorrect email"
      return
    }
    if !Validation.isValidPassword(string: passwordTextField.text) {
      errorLabel.text = "Incorrect Password"
      return
    }
    
    let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    self.presenter?.authenticationCheck(email: email ?? "", password: password ?? "")
    
  }
}

extension LoginViewController: GIDSignInDelegate {
  //MARK: Google Signup Response
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
            withError error: Error!) {
    if error != nil {
      print(error.localizedDescription)
      return
    } else {
      print("Successfully logged into Google", user ?? "")
      
      guard  let idToken = user.authentication.idToken else  {
        return
      }
      
      guard let accessToken = user.authentication.accessToken else { return }
      
      self.presenter?.googleSignIn(idToken: idToken, accessToken: accessToken)
      
    }
  }
}
