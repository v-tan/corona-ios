//
//  LoginWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class LoginWireframe: ViperBaseWireframe, LoginWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: LoginViewController.self,
      presenter: LoginPresenter.self,
      interactor: LoginInteractor.self,
      apiManager: LoginAuthManager.self, localManager: nil)
    return model
  }
  
  func signUpViewController(view: LoginViewProtocol) {
    
    let signUpVc = SignUpWireframe().setupViewController() as!
    SignUpViewController
    (view as? LoginViewController)?.navigationController?.pushViewController(
      signUpVc, animated: true)
  }
  
  func countriesViewController(view: LoginViewProtocol, uid: String) {
    
    let countryVc = CountriesWireframe().setupViewController() as!
    CountriesViewController
    countryVc.uid = uid
    (view as? LoginViewController)?.navigationController?.pushViewController(
      countryVc, animated: false)
    
  }
}
