//
//  LoginPresenter.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class LoginPresenter: ViperBasePresenter, LoginPresenterProtocol {
  
  func googleSignIn(idToken: String, accessToken: String) {
    self.interactor?.googleAuth(idToken: idToken, accessToken: accessToken)
  }
  
  func checkUserExists() {
    self.interactor?.checkUser()
  }
  
  func signUpVc() {
    self.wireframe?.signUpViewController(view: self.view!)
  }
  
  func authenticationCheck(email: String, password: String) {
    self.interactor?.authCheck(email: email, password: password)
  }
}

extension LoginPresenter: LoginInteractorOutputProtocol {
  
  func googleSignInSuccess(uid: String) {
    self.wireframe?.countriesViewController(view: self.view!, uid: uid)
  }
  
  
  func userFound(response: String) {
   self.wireframe?.countriesViewController(view: self.view!, uid: response)
  }
  
  func failure(response: String) {
    self.view?.errorText(error: response)
  }
  
  func success(response: String) {
    self.wireframe?.countriesViewController(view: self.view!, uid: response)
  }
}
