//
//  LoginInteractor.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation
import FirebaseAuth

class LoginInteractor: ViperBaseInteractor, LoginInteractorInputProtocol {
  func googleAuth(idToken: String, accessToken: String) {
    self.apiDataManager?.googleSigning(idToken: idToken, accessToken: accessToken)
  }
  
  func checkUser() {
    self.apiDataManager?.checkUserExistence()
  }
  
  func authCheck(email: String, password: String) {
    self.apiDataManager?.verification(email: email, password: password)
  }
}

extension LoginInteractor: LoginInteractorOutputProtocol {
  
  func googleSignInSuccess(uid: String) {
    self.output?.googleSignInSuccess(uid: uid)
  }
  
  func userFound(response: String) {
    self.output?.userFound(response: response)
  }
  
  func failure(response: String) {
    self.output?.failure(response: response)
  }
  
  func success(response: String) {
    self.output?.success(response: response)
  }
}
