//
//  CountriesProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol CountriesViewProtocol: ViperViewProtocol {
  
  func countryData(data: [CountryModel])
}

extension CountriesViewProtocol {
  var presenter: CountriesPresenterProtocol? {
    return _presenter as? CountriesPresenterProtocol
  }
}

protocol CountriesPresenterProtocol: ViperBasePresenterProtocol {
  func countryReport()
  func getCountry()
  func timelineCharts(cityName: String)
  func cityPresentation(stateName: String)
  func indiaChart()
}

extension CountriesPresenterProtocol {
  var view: CountriesViewProtocol? {
    return _view as? CountriesViewProtocol
  }
  
  var interactor: CountriesInteractorInputProtocol? {
    _interactor as? CountriesInteractorInputProtocol
  }
  
  var wireframe: CountriesWireframeProtocol? {
    return _wireframe as? CountriesWireframeProtocol
  }
}

protocol CountriesInteractorInputProtocol: ViperBaseInteractorInputProtocol {
  
  func fetchCountriesApi()
}

extension CountriesInteractorInputProtocol {
  var output: CountriesInteractorOutputProtocol? {
    return _output as? CountriesInteractorOutputProtocol
  }
  var apiDataManager: CountriesApiManagerInputProtocol? {
    return _apiDataManager as? CountriesApiManagerInputProtocol
  }
}

protocol CountriesInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, CountriesApiManagerOutputProtocol {}

protocol CountriesWireframeProtocol: ViperBaseWireframeProtocol {
  
  func statesViewController(view: CountriesViewProtocol)
  func timeLineChartViewController(view: CountriesViewProtocol, cityName: String)
  func cityDetailsViewController(view: CountriesViewProtocol, stateName: String)
  func indiaChartViewController(view: CountriesViewProtocol)
}


