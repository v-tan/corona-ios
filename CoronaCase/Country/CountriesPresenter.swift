//
//  CountriesPresenter.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class CountriesPresenter: ViperBasePresenter,CountriesPresenterProtocol {
  
  func timelineCharts(cityName: String) {
    self.wireframe?.timeLineChartViewController(view: self.view!, cityName: cityName)
  }
  
  func indiaChart() {
    self.wireframe?.indiaChartViewController(view: self.view!)
  }
  
  func getCountry() {
    self.interactor?.fetchCountriesApi()
  }
  
  func countryReport() {
    self.wireframe?.statesViewController(view: self.view!)
  }
  
  func cityPresentation(stateName: String) {
    self.wireframe?.cityDetailsViewController(view: self.view!, stateName: stateName)
  }
}

extension CountriesPresenter: CountriesInteractorOutputProtocol {
  func success(response: [CountryModel]) {
    self.view?.countryData(data: response)
  }
}

