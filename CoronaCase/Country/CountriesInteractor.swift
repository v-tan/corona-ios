//
//  CountriesInteractor.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class CountriesInteractor: ViperBaseInteractor, CountriesInteractorInputProtocol {
  
  func fetchCountriesApi() {
    self.apiDataManager?.countryList()
  }
}

extension CountriesInteractor: CountriesInteractorOutputProtocol {
  func success(response: [CountryModel]) {
    self.output?.success(response: response)
  }
}
