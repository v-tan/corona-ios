//
//  CountriesWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class CountriesWireframe: ViperBaseWireframe,CountriesWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: CountriesViewController.self,
      presenter: CountriesPresenter.self,
      interactor: CountriesInteractor.self,
      apiManager: CountriesApiManager.self, localManager: nil)
    return model
  }
  
  func indiaChartViewController(view: CountriesViewProtocol) {
    let indiaChartVc = IndiaChartWireframe().setupViewController() as!
    IndiaChartViewController
    (view as? CountriesViewController)?.navigationController?.pushViewController(
      indiaChartVc, animated: true)
  }
  
  func statesViewController(view: CountriesViewProtocol) {
    let stateVc = StatesWireframe().setupViewController() as!
    StatesViewController
    (view as? CountriesViewController)?.navigationController?.pushViewController(
      stateVc, animated: true)
  }
  
  func cityDetailsViewController(view: CountriesViewProtocol, stateName: String) {
    let cityDetailsVc = CityDetailsWireframe().setupViewController() as!
    CityDetailsViewController
    cityDetailsVc.stateName = stateName
    (view as? CountriesViewController)?.navigationController?.pushViewController(
      cityDetailsVc, animated: true)
  }
  
  func timeLineChartViewController(view: CountriesViewProtocol, cityName: String) {
    let timeLineChartVc = TimeLineChartWireframe().setupViewController() as!
    TimeLineChartViewController
    timeLineChartVc.cityName = cityName
    (view as? CountriesViewController)?.navigationController?.pushViewController(
      timeLineChartVc, animated: true)
  }
}
