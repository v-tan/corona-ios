//
//  CountriesViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 18/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import FirebaseAuth

class CountriesViewController: BaseViewController, CountriesViewProtocol {
  
  @IBOutlet var todayDeathsLabel: UILabel!
  @IBOutlet var deathsLabel: UILabel!
  @IBOutlet var casesLabel: UILabel!
  @IBOutlet var todayCasesLabel: UILabel!
  @IBOutlet var recoveredLabel: UILabel!
  @IBOutlet var todayRecoveredLabel: UILabel!
  @IBOutlet var allDetailsButton: UIButton!
  @IBOutlet var titleLabel: UILabel!
  
  @IBOutlet var recoveredHeadingLabel: UILabel!
  @IBOutlet var todayRecoveredHeadingLabel: UILabel!
  @IBOutlet var todayDeathsHeadingLabel: UILabel!
  @IBOutlet var deathsHeadingLabel: UILabel!
  @IBOutlet var todayCasesHeadingLabel: UILabel!
  @IBOutlet var casesHeadingLabel: UILabel!
  
  var countryModel = [CountryModel]()
  var uid: String?
  var check: Bool?
  var stateName: String?
  var noOfCases: Int?
  var deaths: Int?
  var active: Int?
  var cured: Int?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if check == true {
      self.title = " "
      self.titleLabel.text = stateName ?? ""
      recoveredHeadingLabel.isHidden = true
      recoveredLabel.isHidden = true
      todayRecoveredLabel.isHidden = true
      todayRecoveredHeadingLabel.isHidden = true
      
      casesLabel.text = String(noOfCases ?? 0)
      todayCasesHeadingLabel.text = "Active"
      todayCasesLabel.text = String(active ?? 0)
      deathsLabel.text = String(deaths ?? 0)
      todayDeathsHeadingLabel.text = "Cured"
      todayDeathsLabel.text = String(cured ?? 0)
      
    } else  {
      self.title = Constants.countryTitle
      showLoading()
      self.presenter?.getCountry()
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if check == true {
      addLeftBarButton()
    } else {
      addBarButton()
      self.navigationItem.setHidesBackButton(true, animated: true)
    }
  }
  
  func showLoading() {
    let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
    Indicator.label.text = "Loading"
    Indicator.isUserInteractionEnabled = false
    Indicator.show(animated: true)
  }
  
  func countryData(data: [CountryModel]) {
    countryModel = data
    
    for i in 0..<countryModel.count {
      
      if countryModel[i].country == "India" {
        todayRecoveredLabel.text = String(countryModel[i].todayRecovered ?? 0)
        recoveredLabel.text = String(countryModel[i].recovered ?? 0)
        todayDeathsLabel.text = String(countryModel[i].todayDeaths ?? 0)
        deathsLabel.text = String(countryModel[i].deaths ?? 0)
        todayCasesLabel.text = String(countryModel[i].todayCases ?? 0)
        casesLabel.text = String(countryModel[i].cases ?? 0)
      }
    }
    MBProgressHUD.hide(for: self.view, animated: true)
  }
  
  func addBarButton() {
    
    let logOutButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    logOutButton.setTitle("Logout", for: .normal)
    logOutButton.setTitleColor(UIColor.red, for: .normal)
    logOutButton.addTarget(self, action: #selector(logOut), for: UIControl.Event.touchUpInside)
    logOutButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    
    let logOutbarButton = UIBarButtonItem(customView: logOutButton)
    self.navigationItem.leftBarButtonItem = logOutbarButton
  }
  
  
  func addLeftBarButton() {
    self.navigationController?.isNavigationBarHidden = false
    
    let addButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    addButton.setImage(UIImage(systemName: "plus"), for: .normal)
    addButton.addTarget(self, action: #selector(addButtonClicked), for: UIControl.Event.touchUpInside)
    addButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    
    let addBarButton = UIBarButtonItem(customView: addButton)
    self.navigationItem.rightBarButtonItem = addBarButton
    
    
    let logOutButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    logOutButton.setTitle("Back", for: .normal)
    logOutButton.setTitleColor(UIColor.red, for: .normal)
    logOutButton.addTarget(self, action: #selector(back), for: UIControl.Event.touchUpInside)
    logOutButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    
    let logOutbarButton = UIBarButtonItem(customView: logOutButton)
    self.navigationItem.leftBarButtonItem = logOutbarButton
  }
  
  @objc func back() {
    navigationController?.popToRootViewController(animated: true)
  }
  
  @objc func addButtonClicked() {
    self.presenter?.timelineCharts(cityName: self.titleLabel.text ?? "" )
  }
  
  @objc func logOut() {
    do {
      try Auth.auth().signOut()
      
    } catch {
      print("already logged out")
    }
    navigationController?.popToRootViewController(animated: true)
  }
  
  @IBAction func chartButton(_ sender: UIButton) {
    self.presenter?.indiaChart()
  }
  
  @IBAction func allDetailsClicked(_ sender: UIButton) {
    
    if check == true {
      //code for next District screen
      self.presenter?.cityPresentation(stateName: stateName ?? "")
      
    } else {
      self.presenter?.countryReport()
    }
  }
}
