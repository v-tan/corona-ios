//
//  CityDetailsViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 21/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit

class CityDetailsViewController: BaseViewController, CityDetailsViewProtocol {
  
  @IBOutlet var tableView: UITableView!
  var cities: [String] = []
  var districtModel: StateDataModel?
  var districtCases: [Int] = []
  var districtRecovered: [Int] = []
  var districtDeaths: [Int] = []
  var stateName : String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addBarButton()
    guard let url = URL(string: Constants.districtApiUrl ) else { return }
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      
      do {
        if error == nil {
          let district = try JSONDecoder().decode(StateDataModel.self, from: data!)
          
          switch self.stateName {
            
          case "Jammu and Kashmir":
            let array = Array(district.jammuAndKashmir.districtData.keys) // for Dictionary
            self.cities = array
            
            let area = district.jammuAndKashmir.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
              
            }
            
          case "West Bengal" :
            let array = Array(district.westBengal.districtData.keys)
            self.cities = array
            
            let area = district.westBengal.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Uttarakhand" :
            let array = Array(district.uttarakhand.districtData.keys)
            self.cities = array
            
            let area = district.uttarakhand.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Uttar Pradesh" :
            let array = Array(district.uttarPradesh.districtData.keys)
            self.cities = array
            
            let area = district.uttarPradesh.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Telangana" :
            let array = Array(district.telangana.districtData.keys)
            self.cities = array
            
            let area = district.telangana.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Tamil Nadu" :
            let array = Array(district.tamilNadu.districtData.keys)
            self.cities = array
            
            let area = district.tamilNadu.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Rajasthan" :
            let array = Array(district.rajasthan.districtData.keys)
            self.cities = array
            
            let area = district.rajasthan.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Punjab" :
            let array = Array(district.punjab.districtData.keys)
            self.cities = array
            
            let area = district.punjab.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Odisha" :
            let array = Array(district.odisha.districtData.keys)
            self.cities = array
            
            let area = district.odisha.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Nagaland" :
            let array = Array(district.nagaland.districtData.keys)
            self.cities = array
            
            let area = district.nagaland.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Mizoram" :
            let array = Array(district.mizoram.districtData.keys)
            self.cities = array
            
            let area = district.mizoram.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Meghalaya" :
            let array = Array(district.meghalaya.districtData.keys)
            self.cities = array
            
            let area = district.meghalaya.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Manipur" :
            let array = Array(district.manipur.districtData.keys)
            self.cities = array
            
            let area = district.manipur.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Maharashtra" :
            let array = Array(district.maharashtra.districtData.keys)
            self.cities = array
            
            let area = district.maharashtra.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Madhya Pradesh" :
            let array = Array(district.madhyaPradesh.districtData.keys)
            self.cities = array
            
            let area = district.madhyaPradesh.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Kerala":
            let array = Array(district.kerala.districtData.keys)
            self.cities = array
            
            let area = district.kerala.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Karnataka":
            let array = Array(district.karnataka.districtData.keys)
            self.cities = array
            
            let area = district.karnataka.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Jharkhand":
            let array = Array(district.jharkhand.districtData.keys)
            self.cities = array
            
            let area = district.jharkhand.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Himachal Pradesh":
            let array = Array(district.himachalPradesh.districtData.keys)
            self.cities = array
            
            let area = district.himachalPradesh.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Haryana":
            let array = Array(district.haryana.districtData.keys)
            self.cities = array
            
            let area = district.haryana.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Gujarat":
            let array = Array(district.gujarat.districtData.keys)
            self.cities = array
            
            let area = district.gujarat.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Delhi":
            let array = Array(district.delhi.districtData.keys)
            self.cities = array
            
            let area = district.delhi.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Chhattisgarh":
            let array = Array(district.chhattisgarh.districtData.keys)
            self.cities = array
            
            let area = district.chhattisgarh.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Bihar":
            let array = Array(district.bihar.districtData.keys)
            self.cities = array
            
            let area = district.bihar.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Assam":
            let array = Array(district.assam.districtData.keys)
            self.cities = array
            
            let area = district.assam.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Arunachal Pradesh":
            let array = Array(district.arunachalPradesh.districtData.keys)
            self.cities = array
            
            let area = district.arunachalPradesh.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          case "Andhra Pradesh":
            let array = Array(district.andhraPradesh.districtData.keys)
            self.cities = array
            
            let area = district.andhraPradesh.districtData
            
            for i in self.cities {
              let coronaCase = (area[i]?.confirmed), deaths = (area[i]?.deceased), recovery = (area[i]?.recovered)
              self.districtCases.append(coronaCase ?? 0)
              self.districtDeaths.append(deaths ?? 0)
              self.districtRecovered.append(recovery ?? 0)
            }
            
          default: print("no states")
          }
          DispatchQueue.main.async {
            self.districtModel = district
            self.tableView.reloadData()
          }
        }
      } catch {
        print("Error Found")
      }
    }.resume()
  }
  
  func addBarButton() {
    self.navigationController?.isNavigationBarHidden = false
    let addButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    addButton.setImage(UIImage(systemName: "square"), for: .normal)
    addButton.addTarget(self, action: #selector(addButtonClicked), for: UIControl.Event.touchUpInside)
    addButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    
    let addBarButton = UIBarButtonItem(customView: addButton)
    self.navigationItem.rightBarButtonItem = addBarButton
  }
  
  @objc func addButtonClicked() {
    self.presenter?.cityGraph(stateName: stateName ?? "", cities: cities, districtCases: districtCases, districtDeaths: districtDeaths, districtRecovery: districtRecovered)
  }
}

extension CityDetailsViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cities.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "districtcell", for: indexPath)
    cell.textLabel?.text = cities[indexPath.row]
    
    return cell       
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.presenter?.cityCases(cityName: cities[indexPath.row], stateName: stateName ?? "")
  }
}
