//
//  CityDetailsProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 21/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol  CityDetailsViewProtocol: ViperViewProtocol {}

extension CityDetailsViewProtocol {
  var presenter: CityDetailsPresenterProtocol? {
    return _presenter as? CityDetailsPresenterProtocol
  }
}

protocol CityDetailsPresenterProtocol: ViperBasePresenterProtocol {
  
  func cityCases(cityName: String, stateName: String)
  func cityGraph(stateName: String, cities: [String], districtCases: [Int], districtDeaths: [Int], districtRecovery: [Int])
}

extension CityDetailsPresenterProtocol {
  var view: CityDetailsViewProtocol? {
    return _view as? CityDetailsViewProtocol
  }
  
  var interactor: CityDetailsInteractorInputProtocol? {
    _interactor as? CityDetailsInteractorInputProtocol
  }
  
  var wireframe: CityDetailsWireframeProtocol? {
    return _wireframe as? CityDetailsWireframeProtocol
  }
}

protocol CityDetailsInteractorInputProtocol: ViperBaseInteractorInputProtocol {}

extension CityDetailsInteractorInputProtocol {
  var output: CityDetailsInteractorOutputProtocol? {
    return _output as? CityDetailsInteractorOutputProtocol
  }
}

protocol CityDetailsInteractorOutputProtocol: ViperBaseInteractorOutputProtocol {}

protocol CityDetailsWireframeProtocol: ViperBaseWireframeProtocol {
  
  func cityCasesViewController(view: CityDetailsViewProtocol, cityName: String, stateName: String)
  func timeLineChartViewController(view: CityDetailsViewProtocol, stateName: String, cities: [String], districtCases: [Int], districtDeaths: [Int], districtRecovery: [Int])
}

