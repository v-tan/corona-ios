//
//  CityDetailsWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 21/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class CityDetailsWireframe: ViperBaseWireframe, CityDetailsWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: CityDetailsViewController.self,
      presenter: CityDetailsPresenter.self,
      interactor: nil,
      apiManager: nil, localManager: nil)
    return model
  }
  
  func timeLineChartViewController(view: CityDetailsViewProtocol, stateName: String, cities: [String], districtCases: [Int], districtDeaths: [Int], districtRecovery: [Int]) {
    
    let timeLineVc = TimeLineChartWireframe().setupViewController() as!
    TimeLineChartViewController
    timeLineVc.stateName = stateName
    timeLineVc.city = cities
    timeLineVc.check = 0
    timeLineVc.districtCases = districtCases
    timeLineVc.districtDeaths = districtDeaths
    timeLineVc.districtRecovery = districtRecovery
    (view as? CityDetailsViewController)?.navigationController?.pushViewController(
      timeLineVc, animated: true)
  }
  
  func cityCasesViewController(view: CityDetailsViewProtocol, cityName: String, stateName: String) {
    let cityCasesVc = CityCasesWireframe().setupViewController() as!
    CityCasesViewController
    cityCasesVc.cityName = cityName
    cityCasesVc.stateName = stateName
    (view as? CityDetailsViewController)?.navigationController?.pushViewController(
      cityCasesVc, animated: true)
  }
}
