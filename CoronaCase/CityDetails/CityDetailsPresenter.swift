//
//  CityDetailsPresenter.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 21/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class CityDetailsPresenter: ViperBasePresenter, CityDetailsPresenterProtocol {
  
  func cityGraph(stateName: String, cities: [String], districtCases: [Int], districtDeaths: [Int], districtRecovery: [Int]) {
    self.wireframe?.timeLineChartViewController(view: self.view!, stateName: stateName, cities: cities, districtCases: districtCases, districtDeaths: districtDeaths, districtRecovery: districtRecovery)
  }
  
  func cityCases(cityName: String, stateName: String) {
    self.wireframe?.cityCasesViewController(view: self.view!, cityName: cityName, stateName: stateName)
  }
}
