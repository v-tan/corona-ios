//
//  Model.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 18/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

struct CountryModel : Decodable {
  
  var country : String?
  var cases: Int?
  var deaths: Int?
  var recovered: Int?
  var todayCases: Int?
  var todayDeaths: Int?
  var todayRecovered: Int?
}

struct StateModel : Decodable {
  
  var noOfCases : Int?
  var state : String?
  var deaths: Int?
  var active: Int?
  var cured: Int?
}

struct StateTimeLine: Decodable {
  
  var latestDate: Int?
  var state: String?
}

enum  CodingKeys: String, CodingKey {
  
  case latestDate = "2020-08-18"
}

// MARK: - StateModel
struct StateDataModel: Codable {
    let stateUnassigned: StateUnassigned
    let andamanAndNicobarIslands: AndamanAndNicobarIslands
    let andhraPradesh, arunachalPradesh, assam, bihar: AndhraPradesh
    let chandigarh: Chandigarh
    let chhattisgarh, delhi: AndhraPradesh
    let dadraAndNagarHaveliAndDamanAndDiu: DadraAndNagarHaveliAndDamanAndDiu
    let goa: Goa
    let gujarat, himachalPradesh, haryana, jharkhand: AndhraPradesh
    let jammuAndKashmir, karnataka, kerala: AndhraPradesh
    let ladakh: Ladakh
    let lakshadweep: Lakshadweep
    let maharashtra, meghalaya, manipur, madhyaPradesh: AndhraPradesh
    let mizoram, nagaland, odisha, punjab: AndhraPradesh
    let puducherry: Puducherry
    let rajasthan: AndhraPradesh
    let sikkim: Sikkim
    let telangana, tamilNadu: AndhraPradesh
    let tripura: Tripura
    let uttarPradesh, uttarakhand, westBengal: AndhraPradesh

    enum CodingKeys: String, CodingKey {
        case stateUnassigned = "State Unassigned"
        case andamanAndNicobarIslands = "Andaman and Nicobar Islands"
        case andhraPradesh = "Andhra Pradesh"
        case arunachalPradesh = "Arunachal Pradesh"
        case assam = "Assam"
        case bihar = "Bihar"
        case chandigarh = "Chandigarh"
        case chhattisgarh = "Chhattisgarh"
        case delhi = "Delhi"
        case dadraAndNagarHaveliAndDamanAndDiu = "Dadra and Nagar Haveli and Daman and Diu"
        case goa = "Goa"
        case gujarat = "Gujarat"
        case himachalPradesh = "Himachal Pradesh"
        case haryana = "Haryana"
        case jharkhand = "Jharkhand"
        case jammuAndKashmir = "Jammu and Kashmir"
        case karnataka = "Karnataka"
        case kerala = "Kerala"
        case ladakh = "Ladakh"
        case lakshadweep = "Lakshadweep"
        case maharashtra = "Maharashtra"
        case meghalaya = "Meghalaya"
        case manipur = "Manipur"
        case madhyaPradesh = "Madhya Pradesh"
        case mizoram = "Mizoram"
        case nagaland = "Nagaland"
        case odisha = "Odisha"
        case punjab = "Punjab"
        case puducherry = "Puducherry"
        case rajasthan = "Rajasthan"
        case sikkim = "Sikkim"
        case telangana = "Telangana"
        case tamilNadu = "Tamil Nadu"
        case tripura = "Tripura"
        case uttarPradesh = "Uttar Pradesh"
        case uttarakhand = "Uttarakhand"
        case westBengal = "West Bengal"
    }
}

// MARK: - AndamanAndNicobarIslands
struct AndamanAndNicobarIslands: Codable {
    let districtData: AndamanAndNicobarIslandsDistrictData
    let statecode: String
}

// MARK: - AndamanAndNicobarIslandsDistrictData
struct AndamanAndNicobarIslandsDistrictData: Codable {
    let nicobars, northAndMiddleAndaman, southAndaman, unknown: Nicobars

    enum CodingKeys: String, CodingKey {
        case nicobars = "Nicobars"
        case northAndMiddleAndaman = "North and Middle Andaman"
        case southAndaman = "South Andaman"
        case unknown = "Unknown"
    }
}

// MARK: - Nicobars
struct Nicobars: Codable {
    let notes: String
    let active, confirmed, deceased, recovered: Int
    let delta: Delta
}

// MARK: - Delta
struct Delta: Codable {
    let confirmed, deceased, recovered: Int
}

// MARK: - AndhraPradesh
struct AndhraPradesh: Codable {
    let districtData: [String: Nicobars]
    let statecode: String
}

// MARK: - Chandigarh
struct Chandigarh: Codable {
    let districtData: ChandigarhDistrictData
    let statecode: String
}

// MARK: - ChandigarhDistrictData
struct ChandigarhDistrictData: Codable {
    let chandigarh: Nicobars

    enum CodingKeys: String, CodingKey {
        case chandigarh = "Chandigarh"
    }
}

// MARK: - DadraAndNagarHaveliAndDamanAndDiu
struct DadraAndNagarHaveliAndDamanAndDiu: Codable {
    let districtData: DadraAndNagarHaveliAndDamanAndDiuDistrictData
    let statecode: String
}

// MARK: - DadraAndNagarHaveliAndDamanAndDiuDistrictData
struct DadraAndNagarHaveliAndDamanAndDiuDistrictData: Codable {
    let otherState, dadraAndNagarHaveli, daman, diu: Nicobars

    enum CodingKeys: String, CodingKey {
        case otherState = "Other State"
        case dadraAndNagarHaveli = "Dadra and Nagar Haveli"
        case daman = "Daman"
        case diu = "Diu"
    }
}

// MARK: - Goa
struct Goa: Codable {
    let districtData: GoaDistrictData
    let statecode: String
}

// MARK: - GoaDistrictData
struct GoaDistrictData: Codable {
    let otherState, northGoa, southGoa, unknown: Nicobars

    enum CodingKeys: String, CodingKey {
        case otherState = "Other State"
        case northGoa = "North Goa"
        case southGoa = "South Goa"
        case unknown = "Unknown"
    }
}

// MARK: - Ladakh
struct Ladakh: Codable {
    let districtData: LadakhDistrictData
    let statecode: String
}

// MARK: - LadakhDistrictData
struct LadakhDistrictData: Codable {
    let kargil, leh: Nicobars

    enum CodingKeys: String, CodingKey {
        case kargil = "Kargil"
        case leh = "Leh"
    }
}

// MARK: - Lakshadweep
struct Lakshadweep: Codable {
    let districtData: LakshadweepDistrictData
    let statecode: String
}

// MARK: - LakshadweepDistrictData
struct LakshadweepDistrictData: Codable {
    let lakshadweep: Nicobars

    enum CodingKeys: String, CodingKey {
        case lakshadweep = "Lakshadweep"
    }
}

// MARK: - Puducherry
struct Puducherry: Codable {
    let districtData: PuducherryDistrictData
    let statecode: String
}

// MARK: - PuducherryDistrictData
struct PuducherryDistrictData: Codable {
    let karaikal, mahe, puducherry, yanam: Nicobars

    enum CodingKeys: String, CodingKey {
        case karaikal = "Karaikal"
        case mahe = "Mahe"
        case puducherry = "Puducherry"
        case yanam = "Yanam"
    }
}

// MARK: - Sikkim
struct Sikkim: Codable {
    let districtData: SikkimDistrictData
    let statecode: String
}

// MARK: - SikkimDistrictData
struct SikkimDistrictData: Codable {
    let eastSikkim, northSikkim, southSikkim, westSikkim: Nicobars

    enum CodingKeys: String, CodingKey {
        case eastSikkim = "East Sikkim"
        case northSikkim = "North Sikkim"
        case southSikkim = "South Sikkim"
        case westSikkim = "West Sikkim"
    }
}

// MARK: - StateUnassigned
struct StateUnassigned: Codable {
    let districtData: StateUnassignedDistrictData
    let statecode: String
}

// MARK: - StateUnassignedDistrictData
struct StateUnassignedDistrictData: Codable {
    let unassigned: Nicobars

    enum CodingKeys: String, CodingKey {
        case unassigned = "Unassigned"
    }
}

// MARK: - Tripura
struct Tripura: Codable {
    let districtData: TripuraDistrictData
    let statecode: String
}

// MARK: - TripuraDistrictData
struct TripuraDistrictData: Codable {
    let dhalai, gomati, khowai, northTripura: Nicobars
    let sipahijala, southTripura, unokoti, westTripura: Nicobars

    enum CodingKeys: String, CodingKey {
        case dhalai = "Dhalai"
        case gomati = "Gomati"
        case khowai = "Khowai"
        case northTripura = "North Tripura"
        case sipahijala = "Sipahijala"
        case southTripura = "South Tripura"
        case unokoti = "Unokoti"
        case westTripura = "West Tripura"
    }
}

import Foundation

// MARK: - WelcomeElement
struct DateModel: Codable {
    let the20200130, the20200131, the20200201, the20200202: Int
    let the20200203, the20200204, the20200205, the20200206: Int
    let the20200207, the20200208, the20200209, the20200210: Int
    let the20200211, the20200212, the20200213, the20200214: Int
    let the20200215, the20200216, the20200217, the20200218: Int
    let the20200219, the20200220, the20200221, the20200222: Int
    let the20200223, the20200224, the20200225, the20200226: Int
    let the20200227, the20200228, the20200229, the20200301: Int
    let the20200302, the20200303, the20200304, the20200305: Int
    let the20200306, the20200307, the20200308, the20200309: Int
    let the20200310, the20200311, the20200312, the20200313: Int
    let the20200314, the20200315, the20200316, the20200317: Int
    let the20200318, the20200319, the20200320, the20200321: Int
    let the20200322, the20200323, the20200324, the20200325: Int
    let the20200326, the20200327, the20200328, the20200329: Int
    let the20200330, the20200331, the20200401, the20200402: Int
    let the20200403, the20200404, the20200405, the20200406: Int
    let the20200407, the20200408, the20200409, the20200410: Int
    let the20200411, the20200412, the20200413, the20200414: Int
    let the20200415, the20200416, the20200417, the20200418: Int
    let the20200419, the20200420, the20200421, the20200422: Int
    let the20200423, the20200424, the20200425, the20200426: Int
    let the20200427, the20200428, the20200429, the20200430: Int
    let the20200501, the20200502, the20200503, the20200504: Int
    let the20200505, the20200506, the20200507, the20200508: Int
    let the20200509, the20200510, the20200511, the20200512: Int
    let the20200513, the20200514, the20200515, the20200516: Int
    let the20200517, the20200518, the20200519, the20200520: Int
    let the20200521, the20200522, the20200523, the20200524: Int
    let the20200525, the20200526, the20200527, the20200528: Int
    let the20200529, the20200530, the20200531, the20200601: Int
    let the20200602, the20200603, the20200604, the20200605: Int
    let the20200606, the20200607, the20200608, the20200609: Int
    let the20200610, the20200611, the20200612, the20200613: Int
    let the20200614, the20200615, the20200616, the20200617: Int
    let the20200618, the20200619, the20200620, the20200621: Int
    let the20200622, the20200623, the20200624, the20200625: Int
    let the20200626, the20200627, the20200628, the20200629: Int
    let the20200630, the20200701, the20200702, the20200703: Int
    let the20200704, the20200705, the20200706, the20200707: Int
    let the20200708, the20200709, the20200710, the20200711: Int
    let the20200712, the20200713, the20200714, the20200715: Int
    let the20200716, the20200717, the20200718, the20200719: Int
    let the20200720, the20200721, the20200722, the20200723: Int
    let the20200724, the20200725, the20200726, the20200727: Int
    let the20200728, the20200729, the20200730, the20200731: Int
    let the20200801, the20200802, the20200803, the20200804: Int
    let the20200805, the20200806, the20200807, the20200808: Int
    let the20200809, the20200810, the20200811, the20200812: Int
    let the20200813, the20200814, the20200815, the20200816: Int
    let the20200817, the20200818, the20200819, the20200820: Int
    let the20200821: Int
    let stateUT: String

    enum CodingKeys: String, CodingKey {
        case the20200130 = "2020-01-30"
        case the20200131 = "2020-01-31"
        case the20200201 = "2020-02-01"
        case the20200202 = "2020-02-02"
        case the20200203 = "2020-02-03"
        case the20200204 = "2020-02-04"
        case the20200205 = "2020-02-05"
        case the20200206 = "2020-02-06"
        case the20200207 = "2020-02-07"
        case the20200208 = "2020-02-08"
        case the20200209 = "2020-02-09"
        case the20200210 = "2020-02-10"
        case the20200211 = "2020-02-11"
        case the20200212 = "2020-02-12"
        case the20200213 = "2020-02-13"
        case the20200214 = "2020-02-14"
        case the20200215 = "2020-02-15"
        case the20200216 = "2020-02-16"
        case the20200217 = "2020-02-17"
        case the20200218 = "2020-02-18"
        case the20200219 = "2020-02-19"
        case the20200220 = "2020-02-20"
        case the20200221 = "2020-02-21"
        case the20200222 = "2020-02-22"
        case the20200223 = "2020-02-23"
        case the20200224 = "2020-02-24"
        case the20200225 = "2020-02-25"
        case the20200226 = "2020-02-26"
        case the20200227 = "2020-02-27"
        case the20200228 = "2020-02-28"
        case the20200229 = "2020-02-29"
        case the20200301 = "2020-03-01"
        case the20200302 = "2020-03-02"
        case the20200303 = "2020-03-03"
        case the20200304 = "2020-03-04"
        case the20200305 = "2020-03-05"
        case the20200306 = "2020-03-06"
        case the20200307 = "2020-03-07"
        case the20200308 = "2020-03-08"
        case the20200309 = "2020-03-09"
        case the20200310 = "2020-03-10"
        case the20200311 = "2020-03-11"
        case the20200312 = "2020-03-12"
        case the20200313 = "2020-03-13"
        case the20200314 = "2020-03-14"
        case the20200315 = "2020-03-15"
        case the20200316 = "2020-03-16"
        case the20200317 = "2020-03-17"
        case the20200318 = "2020-03-18"
        case the20200319 = "2020-03-19"
        case the20200320 = "2020-03-20"
        case the20200321 = "2020-03-21"
        case the20200322 = "2020-03-22"
        case the20200323 = "2020-03-23"
        case the20200324 = "2020-03-24"
        case the20200325 = "2020-03-25"
        case the20200326 = "2020-03-26"
        case the20200327 = "2020-03-27"
        case the20200328 = "2020-03-28"
        case the20200329 = "2020-03-29"
        case the20200330 = "2020-03-30"
        case the20200331 = "2020-03-31"
        case the20200401 = "2020-04-01"
        case the20200402 = "2020-04-02"
        case the20200403 = "2020-04-03"
        case the20200404 = "2020-04-04"
        case the20200405 = "2020-04-05"
        case the20200406 = "2020-04-06"
        case the20200407 = "2020-04-07"
        case the20200408 = "2020-04-08"
        case the20200409 = "2020-04-09"
        case the20200410 = "2020-04-10"
        case the20200411 = "2020-04-11"
        case the20200412 = "2020-04-12"
        case the20200413 = "2020-04-13"
        case the20200414 = "2020-04-14"
        case the20200415 = "2020-04-15"
        case the20200416 = "2020-04-16"
        case the20200417 = "2020-04-17"
        case the20200418 = "2020-04-18"
        case the20200419 = "2020-04-19"
        case the20200420 = "2020-04-20"
        case the20200421 = "2020-04-21"
        case the20200422 = "2020-04-22"
        case the20200423 = "2020-04-23"
        case the20200424 = "2020-04-24"
        case the20200425 = "2020-04-25"
        case the20200426 = "2020-04-26"
        case the20200427 = "2020-04-27"
        case the20200428 = "2020-04-28"
        case the20200429 = "2020-04-29"
        case the20200430 = "2020-04-30"
        case the20200501 = "2020-05-01"
        case the20200502 = "2020-05-02"
        case the20200503 = "2020-05-03"
        case the20200504 = "2020-05-04"
        case the20200505 = "2020-05-05"
        case the20200506 = "2020-05-06"
        case the20200507 = "2020-05-07"
        case the20200508 = "2020-05-08"
        case the20200509 = "2020-05-09"
        case the20200510 = "2020-05-10"
        case the20200511 = "2020-05-11"
        case the20200512 = "2020-05-12"
        case the20200513 = "2020-05-13"
        case the20200514 = "2020-05-14"
        case the20200515 = "2020-05-15"
        case the20200516 = "2020-05-16"
        case the20200517 = "2020-05-17"
        case the20200518 = "2020-05-18"
        case the20200519 = "2020-05-19"
        case the20200520 = "2020-05-20"
        case the20200521 = "2020-05-21"
        case the20200522 = "2020-05-22"
        case the20200523 = "2020-05-23"
        case the20200524 = "2020-05-24"
        case the20200525 = "2020-05-25"
        case the20200526 = "2020-05-26"
        case the20200527 = "2020-05-27"
        case the20200528 = "2020-05-28"
        case the20200529 = "2020-05-29"
        case the20200530 = "2020-05-30"
        case the20200531 = "2020-05-31"
        case the20200601 = "2020-06-01"
        case the20200602 = "2020-06-02"
        case the20200603 = "2020-06-03"
        case the20200604 = "2020-06-04"
        case the20200605 = "2020-06-05"
        case the20200606 = "2020-06-06"
        case the20200607 = "2020-06-07"
        case the20200608 = "2020-06-08"
        case the20200609 = "2020-06-09"
        case the20200610 = "2020-06-10"
        case the20200611 = "2020-06-11"
        case the20200612 = "2020-06-12"
        case the20200613 = "2020-06-13"
        case the20200614 = "2020-06-14"
        case the20200615 = "2020-06-15"
        case the20200616 = "2020-06-16"
        case the20200617 = "2020-06-17"
        case the20200618 = "2020-06-18"
        case the20200619 = "2020-06-19"
        case the20200620 = "2020-06-20"
        case the20200621 = "2020-06-21"
        case the20200622 = "2020-06-22"
        case the20200623 = "2020-06-23"
        case the20200624 = "2020-06-24"
        case the20200625 = "2020-06-25"
        case the20200626 = "2020-06-26"
        case the20200627 = "2020-06-27"
        case the20200628 = "2020-06-28"
        case the20200629 = "2020-06-29"
        case the20200630 = "2020-06-30"
        case the20200701 = "2020-07-01"
        case the20200702 = "2020-07-02"
        case the20200703 = "2020-07-03"
        case the20200704 = "2020-07-04"
        case the20200705 = "2020-07-05"
        case the20200706 = "2020-07-06"
        case the20200707 = "2020-07-07"
        case the20200708 = "2020-07-08"
        case the20200709 = "2020-07-09"
        case the20200710 = "2020-07-10"
        case the20200711 = "2020-07-11"
        case the20200712 = "2020-07-12"
        case the20200713 = "2020-07-13"
        case the20200714 = "2020-07-14"
        case the20200715 = "2020-07-15"
        case the20200716 = "2020-07-16"
        case the20200717 = "2020-07-17"
        case the20200718 = "2020-07-18"
        case the20200719 = "2020-07-19"
        case the20200720 = "2020-07-20"
        case the20200721 = "2020-07-21"
        case the20200722 = "2020-07-22"
        case the20200723 = "2020-07-23"
        case the20200724 = "2020-07-24"
        case the20200725 = "2020-07-25"
        case the20200726 = "2020-07-26"
        case the20200727 = "2020-07-27"
        case the20200728 = "2020-07-28"
        case the20200729 = "2020-07-29"
        case the20200730 = "2020-07-30"
        case the20200731 = "2020-07-31"
        case the20200801 = "2020-08-01"
        case the20200802 = "2020-08-02"
        case the20200803 = "2020-08-03"
        case the20200804 = "2020-08-04"
        case the20200805 = "2020-08-05"
        case the20200806 = "2020-08-06"
        case the20200807 = "2020-08-07"
        case the20200808 = "2020-08-08"
        case the20200809 = "2020-08-09"
        case the20200810 = "2020-08-10"
        case the20200811 = "2020-08-11"
        case the20200812 = "2020-08-12"
        case the20200813 = "2020-08-13"
        case the20200814 = "2020-08-14"
        case the20200815 = "2020-08-15"
        case the20200816 = "2020-08-16"
        case the20200817 = "2020-08-17"
        case the20200818 = "2020-08-18"
        case the20200819 = "2020-08-19"
        case the20200820 = "2020-08-20"
        case the20200821 = "2020-08-21"
        case stateUT = "State UT"
    }
}

// MARK: - CityDataModel
struct CityDataModel: Codable {
    let casesTimeSeries: [CasesTimeSery]
    let statewise: [Statewise]
    let tested: [Tested]

    enum CodingKeys: String, CodingKey {
        case casesTimeSeries = "cases_time_series"
        case statewise, tested
    }
}

// MARK: - CasesTimeSery
struct CasesTimeSery: Codable {
    let dailyconfirmed, dailydeceased, dailyrecovered, date: String
    let totalconfirmed, totaldeceased, totalrecovered: String
}

// MARK: - Statewise
struct Statewise: Codable {
    let active, confirmed, deaths, deltaconfirmed: String
    let deltadeaths, deltarecovered, lastupdatedtime, migratedother: String
    let recovered, state, statecode, statenotes: String
}

// MARK: - Tested
struct Tested: Codable {
    let cvlqs, cx0B9, d9Ney, db1Zf: String?
    let dcgjs: String?
    let individualstestedperconfirmedcase, positivecasesfromsamplesreported, samplereportedtoday: String
    let source: String
    let source1: String
    let testedasof, testpositivityrate, testsconductedbyprivatelabs, testsperconfirmedcase: String
    let testspermillion, totalindividualstested, totalpositivecases, totalsamplestested: String
    let updatetimestamp: String

    enum CodingKeys: String, CodingKey {
        case cvlqs = "_cvlqs"
        case cx0B9 = "_cx0b9"
        case d9Ney = "_d9ney"
        case db1Zf = "_db1zf"
        case dcgjs = "_dcgjs"
        case individualstestedperconfirmedcase, positivecasesfromsamplesreported, samplereportedtoday, source, source1, testedasof, testpositivityrate, testsconductedbyprivatelabs, testsperconfirmedcase, testspermillion, totalindividualstested, totalpositivecases, totalsamplestested, updatetimestamp
    }
}
