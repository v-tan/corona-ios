//
//  ViperBasePresenter.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol ViperBasePresenterProtocol: ObjectCreatorProtocol{
    var _view: ViperViewProtocol? {get set}
    var _wireframe: ViperBaseWireframeProtocol? {get set}
    var _interactor: ViperBaseInteractorInputProtocol? {get set}
    var _data: ViperTransferData? {get set}
}

class ViperBasePresenter: ViperBasePresenterProtocol {
    weak var _view: ViperViewProtocol?
    var _wireframe: ViperBaseWireframeProtocol?
    var _interactor: ViperBaseInteractorInputProtocol?
    var _data: ViperTransferData?
  
    required init() {}
}

protocol ViperTransferData{}
