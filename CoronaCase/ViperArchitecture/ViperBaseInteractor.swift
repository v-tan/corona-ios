//
//  ViperBaseInteractor.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol ViperBaseInteractorInputProtocol: ObjectCreatorProtocol{
    var _output: ViperBaseInteractorOutputProtocol? {get set}
    var _apiDataManager: ApiDataManagerInputProtocol? {get set}
    var _localDataManager: LocalDataManagerInputProtocol?{get set}
}

class ViperBaseInteractor: ViperBaseInteractorInputProtocol {
    var _apiDataManager: ApiDataManagerInputProtocol?
    var _localDataManager: LocalDataManagerInputProtocol?
    weak var _output: ViperBaseInteractorOutputProtocol?
    
    required init() {}
}

protocol ViperBaseInteractorOutputProtocol: class {}
