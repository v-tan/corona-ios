//
//  DataMangers.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol ApiDataManagerInputProtocol: ObjectCreatorProtocol {
    var _output: ApiDataManagerOutputProtocol? {get set}
}

class ApiDataManager: ApiDataManagerInputProtocol{
    weak var _output: ApiDataManagerOutputProtocol?
    
    required init() {}
}

protocol ApiDataManagerOutputProtocol: class {}

protocol LocalDataManagerInputProtocol: ObjectCreatorProtocol {}

class LocalDataManager: LocalDataManagerInputProtocol {
    required init() {}
}
