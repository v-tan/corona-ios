//
//  ViperBaseWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

import UIKit

protocol ObjectCreatorProtocol: class {
  init()
}

extension ObjectCreatorProtocol {
  static func getSelf() -> Self {
    return Self()
  }
}

protocol ViperBaseWireframeProtocol: ObjectCreatorProtocol {
  var model: ViperModel! {get}
}

class ViperBaseWireframe: NSObject,ViperBaseWireframeProtocol {
  var model: ViperModel! {
    return nil
  }
  
  required override init(){}
  
  func setupViewController(data: ViperTransferData? = nil) -> UIViewController? {
    guard let model = model  else {
      return nil
    }
    
    var view: ViperViewProtocol!
    //check if view is uiviewcontroller type
    if model.view is UIViewController.Type {
      //check for storyboard
      if let story = model.storyBoard{
        
        let vc = UIStoryboard(name: story.rawValue, bundle: nil)
          .instantiateViewController(withIdentifier: String(describing: model.view))
        view = vc as? ViperViewProtocol
        
      }
    }
    //check if view exists
    guard view != nil else {
      return nil
    }
    
    let presenter = model.presenter.getSelf()
    let interactor = model.interactor?.getSelf()
    let apiManager = model.apiManager?.getSelf()
    let localDataManager = model.localManager?.getSelf()
    
    //view connection
    view._presenter = presenter
    
    //presenter connection
    presenter._view = view
    presenter._wireframe = self
    presenter._interactor = interactor
    
    presenter._data = data
    
    //interactor connections
    interactor?._output = presenter as? ViperBaseInteractorOutputProtocol
    interactor?._apiDataManager = apiManager
    interactor?._localDataManager  = localDataManager
    
    //api manager connections
    apiManager?._output = interactor as? ApiDataManagerOutputProtocol
    
    return view as? UIViewController
  }
  
  //navigation name convention should be
  func setupNavigationVc(with data: ViperTransferData? = nil) -> UINavigationController? {
    if let vc = setupViewController(data: data){
      let story = model.storyBoard?.rawValue ?? AppStoryboard.Main.rawValue
      let nav = UIStoryboard(name: story, bundle: nil).instantiateViewController(withIdentifier: "NavigationController") as? UINavigationController
      nav?.viewControllers = [vc]
      return nav
      
    } else {
      return nil
    }
  }
}

struct ViperModel {
  var storyBoard: AppStoryboard?
  var view: ViperViewProtocol.Type
  var presenter: ViperBasePresenterProtocol.Type
  var interactor: ViperBaseInteractorInputProtocol.Type?
  var apiManager: ApiDataManagerInputProtocol.Type?
  var localManager: LocalDataManagerInputProtocol.Type?
}
