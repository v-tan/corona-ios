//
//  ViperBaseView.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation
import UIKit

protocol ViperViewProtocol: ObjectCreatorProtocol {
  var _presenter: ViperBasePresenterProtocol? {get set}
}

class BaseViewController: UIViewController, ViperViewProtocol {
  var _presenter: ViperBasePresenterProtocol?
}
