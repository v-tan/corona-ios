//
//  SignUpViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseAuth

class SignUpViewController: BaseViewController, SignUpViewProtocol {
  
  @IBOutlet var backToLoginButton: UIButton!
  @IBOutlet weak var googleLoginButton: UIButton!
  @IBOutlet weak var googleSignInView: UIView!
  @IBOutlet var firstNameTextField: UITextField!
  @IBOutlet var lastNameTextField: UITextField!
  @IBOutlet var emailTextField: UITextField!
  @IBOutlet var passwordTextField: UITextField!
  @IBOutlet var errorLabel: UILabel!
  @IBOutlet var signUpButton: UIButton!
  var isPresent: Bool?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    initialSetUp()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  func initialSetUp() {
    googleSignInView.layer.masksToBounds = true
    signUpButton.layer.masksToBounds = true
    signUpButton.layer.cornerRadius = self.view.frame.height/32
    googleSignInView.layer.cornerRadius = self.view.frame.height/32
    signUpButton.layer.borderWidth = 1
    signUpButton.layer.borderColor = UIColor.black.cgColor
    googleSignInView.layer.borderWidth = 1
    googleSignInView.layer.borderColor = UIColor.black.cgColor
  }
  
  func googleSignInSetup() {
    GIDSignIn.sharedInstance().delegate = self
    GIDSignIn.sharedInstance()?.presentingViewController = self
    let googleSignInButton = GIDSignInButton()
    self.googleSignInView.addSubview(googleSignInButton)
  }
  
  func errorText(error: String) {
    errorLabel.text = error
  }
  
  @IBAction func loginClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func googleLoginClicked(_ sender: UIButton) {
    GIDSignIn.sharedInstance().delegate = self
    GIDSignIn.sharedInstance()?.presentingViewController = self
    GIDSignIn.sharedInstance()?.signIn()
  }
  
  @IBAction func signUpButtonTapped(_ sender: UIButton) {
    
    if !Validation.isValidName(string: firstNameTextField.text) {
      errorLabel.text = "Incorrect FirstName"
      return
    }
    
    if !Validation.isValidName(string: lastNameTextField.text) {
      errorLabel.text = "Incorrect LastName"
      return
    }
    
    if !Validation.isValidEmail(string: emailTextField.text) {
      errorLabel.text = "Incorrect email"
      return
    }
    
    if !Validation.isValidPassword(string: passwordTextField.text) {
      errorLabel.text = "Incorrect Password"
      return
    }
    
    let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    
    self.presenter?.signingUp(email: email ?? "", password: password ?? "")
  }
}

extension SignUpViewController: GIDSignInDelegate {
  //MARK: Google Signup Response
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
            withError error: Error!) {
    if error != nil {
      print(error.localizedDescription)
      return
    } else {
      print("Successfully logged into Google", user ?? "")
      
      guard  let idToken = user.authentication.idToken else  {
        return
      }
      
      guard let accessToken = user.authentication.accessToken else { return }
      
      self.presenter?.googleSignIn(idToken: idToken, accessToken: accessToken)
    }
  }
}
