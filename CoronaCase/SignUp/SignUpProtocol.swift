//
//  SignUpProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol SignUpViewProtocol: ViperViewProtocol {
  func errorText(error: String)
}

extension SignUpViewProtocol {
  var presenter: SignUpPresenterProtocol? {
    return _presenter as? SignUpPresenterProtocol
  }
}

protocol SignUpPresenterProtocol: ViperBasePresenterProtocol {
  func signingUp(email: String, password: String)
  func googleSignIn(idToken : String, accessToken: String)
}

extension SignUpPresenterProtocol {
  var view: SignUpViewProtocol? {
    return _view as? SignUpViewProtocol
  }
  
  var interactor: SignUpInteractorInputProtocol? {
    _interactor as? SignUpInteractorInputProtocol
  }
  
  var wireframe: SignUpWireframeProtocol? {
    return _wireframe as? SignUpWireframeProtocol
  }
}

protocol SignUpInteractorInputProtocol: ViperBaseInteractorInputProtocol {
  func createUser(email: String, password: String)
  func googleAuth(idToken: String, accessToken: String)
}

extension SignUpInteractorInputProtocol {
  var output: SignUpInteractorOutputProtocol? {
    return _output as? SignUpInteractorOutputProtocol
  }
  var apiDataManager: SignUpAuthManagerInputProtocol? {
    return _apiDataManager as? SignUpAuthManagerInputProtocol
  }
}

protocol SignUpInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, SignUpAuthManagerOutputProtocol
{}
protocol SignUpWireframeProtocol: ViperBaseWireframeProtocol {
  
  func countriesViewController(view: SignUpViewProtocol, uid: String)
}

