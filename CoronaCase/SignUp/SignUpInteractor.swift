//
//  SignUpInteractor.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 20/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class SignUpInteractor: ViperBaseInteractor, SignUpInteractorInputProtocol {
  
  func googleAuth(idToken: String, accessToken: String) {
    self.apiDataManager?.googlesignedIn(idToken: idToken, accessToken: accessToken)
  }
  
  func createUser(email: String, password: String) {
    self.apiDataManager?.signUp(email: email, password: password)
  }
}

extension SignUpInteractor: SignUpInteractorOutputProtocol {
  func googleSignInSuccess(uid: String) {
    self.output?.googleSignInSuccess(uid: uid)
  }
  
  func failure(response: String) {
    self.output?.failure(response: response)
  }
  
  func success(response: String) {
    self.output?.success(response: response)
  }
}
