//
//  SignUpPresenter.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class SignUpPresenter: ViperBasePresenter,SignUpPresenterProtocol {
  
  func googleSignIn(idToken: String, accessToken: String) {
    self.interactor?.googleAuth(idToken: idToken, accessToken: accessToken)
  }
  
  func signingUp(email: String, password: String) {
    self.interactor?.createUser(email: email, password: password)
  }
}


extension SignUpPresenter: SignUpInteractorOutputProtocol {
  
  func googleSignInSuccess(uid: String) {
    self.wireframe?.countriesViewController(view: self.view!, uid: uid)
  }
  
  func failure(response: String) {
    self.view?.errorText(error: response)
  }
  
  func success(response: String) {
    self.wireframe?.countriesViewController(view: self.view!, uid: response)
  }
}
