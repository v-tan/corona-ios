//
//  SignUpWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class SignUpWireframe: ViperBaseWireframe, SignUpWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: SignUpViewController.self,
      presenter: SignUpPresenter.self,
      interactor: SignUpInteractor.self,
      apiManager: SignUpAuthManager.self, localManager: nil)
    return model
  }
  
  func countriesViewController(view: SignUpViewProtocol, uid: String) {
    
    let countryVc = CountriesWireframe().setupViewController() as!
    CountriesViewController
    countryVc.uid = uid
    (view as? SignUpViewController)?.navigationController?.pushViewController(
      countryVc, animated: true)
  }
}
