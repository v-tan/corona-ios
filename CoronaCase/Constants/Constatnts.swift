//
//  Constatnts.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

struct Constants {
  
  static let countryUrl = "https://corona.lmao.ninja/v2/countries?yesterday=&sort=" ,
  indianStateUrl = "https://covid-india-cases.herokuapp.com/states",
  countryTitle = "Countries - Cases",
  statesTitle = "States",
  stateName = "Dadra and Nagar Haveli and Daman and Diu",
  districtApiUrl = "https://api.covid19india.org/state_district_wise.json",
  timelineStateUrl = "https://covid-india-cases.herokuapp.com/statetimeline/"
  
}
