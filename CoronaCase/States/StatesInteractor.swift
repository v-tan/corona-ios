//
//  StatesInteractor.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class StatesInteractor: ViperBaseInteractor, StatesInteractorInputProtocol {
  
  func fetchStatesApi() {
    self.apiDataManager?.stateList()
  }
}

extension StatesInteractor: StatesInteractorOutputProtocol {
  
  func success(response: [StateModel]) {
    self.output?.success(response: response)
  }
}
