//
//  StatesViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 18/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit
import MBProgressHUD

class StatesViewController: BaseViewController , StatesViewProtocol{
  
  @IBOutlet var tableView: UITableView!
  var stateModel = [StateModel]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = Constants.statesTitle
    addBarButton()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    showLoading()
    self.presenter?.getData()
  }
  
  func showLoading() {
    let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
    Indicator.label.text = "Loading"
    Indicator.isUserInteractionEnabled = false
    Indicator.show(animated: true)
    tableView.isUserInteractionEnabled = false
  }
  
  func stateData(data: [StateModel]) {
    stateModel = data
    MBProgressHUD.hide(for: self.view, animated: true)
    tableView.isUserInteractionEnabled = true
    tableView.reloadData()
  }
  
  func addBarButton() {
    self.navigationController?.isNavigationBarHidden = false
    let addButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    addButton.setImage(UIImage(systemName: "square"), for: .normal)
    addButton.addTarget(self, action: #selector(addButtonClicked), for: UIControl.Event.touchUpInside)
    addButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
    
    let addBarButton = UIBarButtonItem(customView: addButton)
    self.navigationItem.rightBarButtonItem = addBarButton
  }
  
  @objc func addButtonClicked() {
    self.presenter?.chartPresentation(model: stateModel)
  }
}

extension StatesViewController: UITableViewDelegate , UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return stateModel.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "statecell", for: indexPath) as! StateCaseTableViewCell
    
    if stateModel[indexPath.row].state ==  Constants.stateName {
      cell.textLabel?.text = "Daman and Diu"
      
    } else {
      cell.textLabel?.text = stateModel[indexPath.row].state
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    self.presenter?.showStateDetails(stateName: stateModel[indexPath.row].state ?? "", deaths:  stateModel[indexPath.row].deaths ?? 0, noOfCases:  stateModel[indexPath.row].noOfCases ?? 0, active:  stateModel[indexPath.row].active ?? 0, cured:  stateModel[indexPath.row].cured ?? 0)
  }
}
