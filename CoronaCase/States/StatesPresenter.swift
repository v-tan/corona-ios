//
//  StatesPresenter.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class StatesPresenter: ViperBasePresenter,StatesPresenterProtocol {
  func chartPresentation(model: [StateModel]) {
    self.wireframe?.chartPresentationViewController(view: self.view!, model: model)
  }
  
  func showStateDetails(stateName: String, deaths: Int, noOfCases: Int, active: Int, cured: Int) {
    self.wireframe?.showCountryViewController(view: self.view!, stateName: stateName, deaths: deaths, noOfCases: noOfCases, active: active, cured: cured)
  }
  
  func getData() {
    self.interactor?.fetchStatesApi()
  }
}

extension StatesPresenter: StatesInteractorOutputProtocol {
  
  func success(response: [StateModel]) {
    self.view?.stateData(data: response)
  }
}

