//
//  StateCaseTableViewCell.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 18/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit

class StateCaseTableViewCell: UITableViewCell {

  @IBOutlet var stateCasesLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
