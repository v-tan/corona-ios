//
//  StatesProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol StatesViewProtocol: ViperViewProtocol {
  
  func stateData(data: [StateModel])
}

extension StatesViewProtocol {
  var presenter: StatesPresenterProtocol? {
    return _presenter as? StatesPresenterProtocol
  }
}

protocol StatesPresenterProtocol: ViperBasePresenterProtocol {
  func getData()
  func showStateDetails(stateName: String, deaths: Int, noOfCases: Int, active: Int, cured: Int)
  func chartPresentation(model: [StateModel])
}

extension StatesPresenterProtocol {
  var view: StatesViewProtocol? {
    return _view as? StatesViewProtocol
  }
  
  var interactor: StatesInteractorInputProtocol? {
    _interactor as? StatesInteractorInputProtocol
  }
  
  var wireframe: StatesWireframeProtocol? {
    return _wireframe as? StatesWireframeProtocol
  }
}

protocol StatesInteractorInputProtocol: ViperBaseInteractorInputProtocol {
  
  func fetchStatesApi()
}

extension StatesInteractorInputProtocol {
  var output: StatesInteractorOutputProtocol? {
    return _output as? StatesInteractorOutputProtocol
  }
  var apiDataManager: StatesApiManagerInputProtocol? {
    return _apiDataManager as? StatesApiManagerInputProtocol
  }
}

protocol StatesInteractorOutputProtocol: ViperBaseInteractorOutputProtocol , StatesApiManagerOutputProtocol{}

protocol StatesWireframeProtocol: ViperBaseWireframeProtocol {
  func showCountryViewController(view: StatesViewProtocol, stateName: String, deaths: Int, noOfCases: Int, active: Int, cured: Int)
  
  func chartPresentationViewController(view: StatesViewProtocol, model: [StateModel])
}


