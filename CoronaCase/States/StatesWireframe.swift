//
//  StatesWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 19/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class StatesWireframe: ViperBaseWireframe,StatesWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: StatesViewController.self,
      presenter: StatesPresenter.self,
      interactor: StatesInteractor.self,
      apiManager: StatesApiManager.self, localManager: nil)
    return model
  }
  
  func showCountryViewController(view: StatesViewProtocol, stateName: String, deaths: Int, noOfCases: Int, active: Int, cured: Int) {
    
    let countryVc = CountriesWireframe().setupViewController() as!
    CountriesViewController
    countryVc.check = true
    countryVc.stateName = stateName
    countryVc.deaths = deaths
    countryVc.noOfCases = noOfCases
    countryVc.active = active
    countryVc.cured = cured
    (view as? StatesViewController)?.navigationController?.pushViewController(
      countryVc, animated: true)
  }
  
  func chartPresentationViewController(view: StatesViewProtocol, model: [StateModel]) {
    let chartVc = ChartPresentationWireframe().setupViewController() as!
    ChartPresentationViewController
    chartVc.statesModel = model
    (view as? StatesViewController)?.navigationController?.pushViewController(
      chartVc, animated: true)
  }
}
