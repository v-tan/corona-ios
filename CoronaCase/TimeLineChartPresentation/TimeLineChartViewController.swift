//
//  TimeLineChartViewController.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 21/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import UIKit
import Alamofire
import Charts
import MBProgressHUD

class TimeLineChartViewController: BaseViewController , TimeLineChartViewProtocol,
ChartPresentationViewProtocol, ChartViewDelegate {
  
  @IBOutlet var chartButton: UIButton!
  
  var cityName: String?
  var time: [DateModel] = []
  var lineChart = LineChartView()
  var city: [String] = []
  var stateName: String?
  var check = 1
  var districtCases: [Int] = []
  var districtDeaths: [Int] = []
  var districtRecovery: [Int] = []
  
  var data = LineChartData()
  var lineChartEntry2 = [ChartDataEntry]()
  var lineChartEntry3 = [ChartDataEntry]()
  var lineChartEntry1 = [ChartDataEntry]()
  
  var newArray : [IChartDataSet] = []
  
  @IBOutlet var checkBoxView: UIView!
  
  var dateCases: [Int] = []
  var date: [String] = ["the20200821" , "the20200820" , "the20200819" , "the20200818" , "the20200817" , "the20200816" , "the20200815" , "the20200814" , "the20200813" , "the20200812" , "the20200811" , "the20200810" , "the20200809" , "the20200808" , "the20200807" , "the20200806" , "the20200805" , "the20200804" , "the20200803" , "the20200802" , "the20200801"]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    checkBoxView.isHidden = true
    showLoading()
    date =  date.reversed()
    
    guard let url = URL(string: "https://covid-india-cases.herokuapp.com/statetimeline/") else { return }
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      
      do {
        if error == nil {
          self.time = try JSONDecoder().decode([DateModel].self, from: data!)
          
          for name in self.time {
            if name.stateUT == self.cityName {
              
              self.dateCases.append(name.the20200801)
              self.dateCases.append(name.the20200802)
              self.dateCases.append(name.the20200803)
              self.dateCases.append(name.the20200804)
              self.dateCases.append(name.the20200805)
              self.dateCases.append(name.the20200806)
              self.dateCases.append(name.the20200807)
              self.dateCases.append(name.the20200808)
              self.dateCases.append(name.the20200809)
              self.dateCases.append(name.the20200810)
              self.dateCases.append(name.the20200811)
              self.dateCases.append(name.the20200812)
              self.dateCases.append(name.the20200813)
              self.dateCases.append(name.the20200814)
              self.dateCases.append(name.the20200815)
              self.dateCases.append(name.the20200816)
              self.dateCases.append(name.the20200817)
              self.dateCases.append(name.the20200818)
              self.dateCases.append(name.the20200819)
              self.dateCases.append(name.the20200820)
              self.dateCases.append(name.the20200821)
            }
          }
          
          DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
          }
        }
      } catch {
        print("Error Found")
      }
    }.resume()
  }
  
  func showLoading() {
    let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
    Indicator.label.text = "Loading"
    Indicator.isUserInteractionEnabled = false
    Indicator.show(animated: true)
  }
  
  @IBAction func chartButtonTapped(_ sender: UIButton) {
    
    lineChart.delegate = self
    lineChart.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width)
    lineChart.center = view.center
    view.addSubview(lineChart)
    
    var entries = [ChartDataEntry]()
    
    if check == 0 {
      
      checkBoxView.isHidden = false
      
      for i in 0..<districtCases.count {
        lineChartEntry1.append(ChartDataEntry(x:Double(i), y:  Double(districtDeaths[i])))
      }
      
      let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Death Dataset")
      data.addDataSet(line1)
      
      line1.colors = [UIColor.black]
      line1.drawValuesEnabled = false
      line1.drawCirclesEnabled = false
      
      if (districtCases.count > 0) {
        
        for i in 0..<districtCases.count {
          lineChartEntry2.append(ChartDataEntry(x:Double(i), y:  Double(districtCases[i])))
        }
        
        let line2 = LineChartDataSet(entries: lineChartEntry2, label: "Cases Dataset")
        data.addDataSet(line2)
        
        line2.colors = [UIColor.red]
        line2.drawValuesEnabled = false
        line2.drawCirclesEnabled = false
      }
      
      if (districtCases.count > 0) {
        
        for i in 0..<districtCases.count {
          lineChartEntry3.append(ChartDataEntry(x:Double(i), y:  Double(districtRecovery[i])))
        }
        
        let line3 = LineChartDataSet(entries: lineChartEntry3, label: "Recovered Dataset")
        data.addDataSet(line3)
        
        line3.colors = [UIColor.green]
        line3.drawValuesEnabled = false
        line3.drawCirclesEnabled = false
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: city)
      self.lineChart.data = data
      
    } else {
      for i in 0..<date.count {
        entries.append(ChartDataEntry(x:Double(i), y:  Double( dateCases[i])))
        lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: date)
        
        let set = LineChartDataSet(entries: entries, label: "Total cases in August")
        
        set.colors = ChartColorTemplates.joyful()
        set.drawValuesEnabled = false
        set.drawCirclesEnabled = false
        data = LineChartData(dataSet: set)
      }
    }
    data.setDrawValues(true)
    lineChart.xAxis.granularity = 1
    lineChart.animate(xAxisDuration: 2.5)
    lineChart.data = data
    lineChart.xAxis.labelPosition = .bottom
    lineChart.xAxis.drawGridLinesEnabled = false
    lineChart.chartDescription?.enabled = false
    lineChart.legend.enabled = true
    lineChart.rightAxis.enabled = false
    lineChart.leftAxis.drawGridLinesEnabled = false
    lineChart.leftAxis.drawLabelsEnabled = true
  }
  
  @IBAction func deathButtonClicked(_ sender: UIButton) {
    
    if sender.isSelected {
      sender.isSelected = false
      
      newArray = []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        
        if "Death Dataset" == (newArray[index].label) {
          
          data.removeDataSetByIndex(index)
        }
      }
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: city)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      for i in 0..<districtCases.count {
        lineChartEntry1.append(ChartDataEntry(x:Double(i), y:  Double(districtDeaths[i])))
      }
      
      let line1 = LineChartDataSet(entries: lineChartEntry1, label: "Death Dataset")
      data.addDataSet(line1)
      
      line1.colors = [UIColor.black]
      line1.drawValuesEnabled = false
      line1.drawCirclesEnabled = false
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: city)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
    }
  }
  
  @IBAction func casesButtoncClicked(_ sender: UIButton) {
    
    if sender.isSelected {
      sender.isSelected = false
      
      newArray =  []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        
        if "Cases Dataset" == (newArray[index].label) {
          data.removeDataSetByIndex(index)
        }
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: city)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      
      if (districtCases.count > 0) {
        
        for i in 0..<districtCases.count {
          lineChartEntry2.append(ChartDataEntry(x:Double(i), y:  Double(districtCases[i])))
        }
        
        let line2 = LineChartDataSet(entries: lineChartEntry3, label: "Cases Dataset")
        data.addDataSet(line2)
        
        line2.colors = [UIColor.red]
        line2.drawValuesEnabled = false
        line2.drawCirclesEnabled = false
        
        self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: city)
        self.lineChart.data = data
        
        data.setDrawValues(true)
        lineChart.xAxis.granularity = 1
        lineChart.animate(xAxisDuration: 2.5)
        lineChart.xAxis.labelPosition = .bottom
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.chartDescription?.enabled = false
        lineChart.legend.enabled = true
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawLabelsEnabled = true
      }
    }
  }
  
  @IBAction func cureButtonClicked(_ sender: UIButton) {
    
    if sender.isSelected {
      sender.isSelected = false
      
      newArray = []
      
      for i in 0..<data.dataSetCount {
        guard let x =  data.getDataSetByIndex(i) else { return }
        
        newArray.append(x)
      }
      
      for index in 0..<newArray.count {
        if "Recovered Dataset" == (newArray[index].label) {
          
          data.removeDataSetByIndex(index)
        }
      }
      
      self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: city)
      self.lineChart.data = data
      
      data.setDrawValues(true)
      lineChart.xAxis.granularity = 1
      lineChart.animate(xAxisDuration: 2.5)
      lineChart.xAxis.labelPosition = .bottom
      lineChart.xAxis.drawGridLinesEnabled = false
      lineChart.chartDescription?.enabled = false
      lineChart.legend.enabled = true
      lineChart.rightAxis.enabled = false
      lineChart.leftAxis.drawGridLinesEnabled = false
      lineChart.leftAxis.drawLabelsEnabled = true
      
    } else {
      sender.isSelected = true
      
      if (districtCases.count > 0) {
        
        for i in 0..<districtCases.count {
          lineChartEntry3.append(ChartDataEntry(x:Double(i), y:  Double(districtRecovery[i])))
        }
        
        let line3 = LineChartDataSet(entries: lineChartEntry2, label: "Recovered Dataset")
        data.addDataSet(line3)
        
        line3.colors = [UIColor.green]
        line3.drawValuesEnabled = false
        line3.drawCirclesEnabled = false
        self.lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: city)
        self.lineChart.data = data
        data.setDrawValues(true)
        lineChart.xAxis.granularity = 1
        lineChart.animate(xAxisDuration: 2.5)
        lineChart.xAxis.labelPosition = .bottom
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.chartDescription?.enabled = false
        lineChart.legend.enabled = true
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawLabelsEnabled = true
      }
    }
  }
}
