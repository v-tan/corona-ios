//
//  TimeLineChartWireframe.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 21/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

class TimeLineChartWireframe: ViperBaseWireframe, TimeLineChartWireframeProtocol {
  
  override var model: ViperModel! {
    let model = ViperModel(
      storyBoard: AppStoryboard.Main,
      view: TimeLineChartViewController.self,
      presenter: TimeLineChartPresenter.self,
      interactor: nil,
      apiManager: nil, localManager: nil)
    return model
  }
}
