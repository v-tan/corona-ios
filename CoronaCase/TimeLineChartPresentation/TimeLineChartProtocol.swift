//
//  TimeLineChartProtocol.swift
//  CoronaCase
//
//  Created by Nitin Kumar Singh on 21/08/20.
//  Copyright © 2020 Nitin Singh. All rights reserved.
//

import Foundation

protocol  TimeLineChartViewProtocol: ViperViewProtocol {}

extension TimeLineChartViewProtocol {
  var presenter: TimeLineChartPresenterProtocol? {
    return _presenter as? TimeLineChartPresenterProtocol
  }
}

protocol TimeLineChartPresenterProtocol: ViperBasePresenterProtocol {}

extension TimeLineChartPresenterProtocol {
  var view: TimeLineChartViewProtocol? {
    return _view as? TimeLineChartViewProtocol
  }
  
  var interactor: TimeLineChartInteractorInputProtocol? {
    _interactor as? TimeLineChartInteractorInputProtocol
  }
  
  var wireframe: TimeLineChartWireframeProtocol? {
    return _wireframe as? TimeLineChartWireframeProtocol
  }
}

protocol TimeLineChartInteractorInputProtocol: ViperBaseInteractorInputProtocol {}

extension TimeLineChartInteractorInputProtocol {
  var output: TimeLineChartInteractorOutputProtocol? {
    return _output as? TimeLineChartInteractorOutputProtocol
  }
}

protocol TimeLineChartInteractorOutputProtocol: ViperBaseInteractorOutputProtocol {}

protocol TimeLineChartWireframeProtocol: ViperBaseWireframeProtocol {}

